from django.db import models

# Create your models here.
from movies.models import movies
from movie_persons.models import movie_persons

class movie_wlinks(models.Model):
	movie_id = models.ForeignKey(movies,unique=True)
	wtitle = models.CharField(max_length=500)

	def __unicode__(self):
		return self.movie_id.movie_name


class person_wlinks(models.Model):
	person_id =models.ForeignKey(movie_persons)
	wtitle = models.CharField(max_length=500)

	def __unicode__(self):
		return self.person_id.person_name +" - "+self.wtitle
