from django.core.files import File
from django.core.files.temp import NamedTemporaryFile



from django.http import HttpResponse
import urllib2
import json
import re
from wiki_parser.models import movie_wlinks
from wiki_parser.models import person_wlinks
from django.contrib.auth.models import *

from django.shortcuts import render_to_response
from django.template import RequestContext
from movie_persons.models import movie_persons
from images.models import profile_images
from movies.models import movies
import random

from bs4 import BeautifulSoup
from bs4 import UnicodeDammit



def person_parser_add(request):
	outputx={}
	if request.user.is_staff:
		pass
	else:
		output = "user not staff"
		return render_to_response('wiki_parser/templates/person_parser_add.html',
								{
									'output':output,
								},context_instance=RequestContext(request)
								)
	output = ''
	try:
		wlink=request.GET['wlink']
	except Exception, e:
		output='no link in url'
		return render_to_response('wiki_parser/templates/person_parser_add.html',
								{
									'output':output,
								},context_instance=RequestContext(request)
								)

	url = """http://en.wikipedia.org/w/api.php?action=parse&page="""+str(wlink)+"""&format=json&prop=text&section=0"""#
	url = re.sub(' ','_',url)
	#print url
	f = urllib2.urlopen(url)
	a=json.loads(f.read())	
	output = a['parse']['text']['*']
	soup = BeautifulSoup(output)
	text=''
	for ptext in soup.find_all('p'):
		text += ptext.get_text()
	dammit = UnicodeDammit(text)
	output = dammit.unicode_markup
	output = re.sub('Cite error: There are <ref> tags on this page, but the references will not show without a {{Reflist}} template or a <references /> tag; see the help ','',output)
	output = re.sub('\[\d+\]','',output)
	outputx['info']=output
#	print output
	url="""http://en.wikipedia.org/w/api.php?action=query&titles="""+str(wlink)+"""&generator=images&gimlimit=10&prop=imageinfo&iiprop=url&format=json"""
	url = re.sub(' ','_',url)
#	print url
	f = urllib2.urlopen(url)
	a=json.loads(f.read())
	if a!=[]:
		wimages=a['query']['pages']
	else:
		wimages=[]
	output = output+'<br/>'
	images=[]
	for image in wimages:
		if int(image)>0:
			pass
		else:
			output = output + '<a href=\''+str(wimages[image]['imageinfo'][0]['url'])+'\' target=\'_blank\'><img src=\''+str(wimages[image]['imageinfo'][0]['url'])+'\' height=\'100px\' /></a>'
			images.append(str(wimages[image]['imageinfo'][0]['url']))
	outputx['images']=images

	url = """http://en.wikipedia.org/w/api.php?action=parse&page="""+str(wlink)+"""&format=json&prop=text"""#
	url = re.sub(' ','_',url)
	print url
	fx = urllib2.urlopen(url)
	ax=json.loads(fx.read())	
	output_x= ax['parse']['text']['*']
	soupx = BeautifulSoup(output_x)
	textx='<p>'
	try:
		for ptextx in soupx.find_all('p'):
			txtx = ptextx.get_text()
			txtx = re.sub('\[\d+\]','',txtx)
			dammit = UnicodeDammit(txtx)
			txtx=dammit.unicode_markup
			textx=textx+txtx+' </p><p> '
	except Exception, e:
		pass
	
	textx=textx+'</p>'
		
	return render_to_response('wiki_parser/templates/person_parser_add.html',
								{
									'output':output,
									'results':outputx,
									'person_wlink':str(wlink),
									'info':textx,
								},context_instance=RequestContext(request)
							 )

from django.contrib.auth.models import User

def person_parser_adding(request):
	if request.user.is_staff:
		pass
	else:
		output = "user not staff"
		return HttpResponse('output')
	output = []
	out={}
	
	try:
		wiki_user=User.objects.get(username='wiki_parser')
		wiki_user_id=int(wiki_user.id)
	except Exception, e:
		return HttpResponse('user: wiki_parser does not exist. create a new user with username: wiki_parser to continue')
	
	outputx = json.dumps(request.POST)
	image_urls=[]
	post = request.POST
	x=0
	if len(request.POST)>7:
		while (x< (len(request.POST)-6)):
			try:
				this_url=post['imagefile'+str(x)]
				if this_url != '':
					image_urls.append(this_url)
			except Exception, e:
				pass
			
			x=x+1
	new_person = movie_persons()
	try:
		new_person.person_name = post['person_name']
	except Exception, e:
		return HttpResponse('error at getting names')
	try:
		new_person.person_date_of_birth=post['person_date_of_birth']
	except Exception, e:
		return HttpResponse('error at getting dob')

	try:
		new_person.person_bio=post['person_bio']
	except Exception, e:
		return HttpResponse('error at getting bio')
	try:
		new_person.person_info=post['person_info']
	except Exception, e:
		return HttpResponse('error at getting info')

	try:
		new_person.person_gender=post['person_gender']
	except Exception, e:
		return HttpResponse('error at getting gender')
	try:
		new_person.person_sp=post['person_sp']
	except Exception, e:
		return HttpResponse('error at getting person_sp')
	new_person_wlink=person_wlinks()
	
	try:
		new_person_wlink.wtitle=re.sub(' ','_',post['person_wlink'])
	except Exception, e:
		return HttpResponse('error at getting person_wlink')
	try:
		new_person.save()
		output.append('person saved sucessfully')
	except Exception, e:
		return HttpResponse('error at saving new_person')
	try:
		new_person_wlink.person_id=new_person
	except Exception, e:
		return HttpResponse('error at getting new_person id')
	try:
		new_person_wlink.save()
		output.append('link saved sucessfully')
	except Exception, e:
		output.append('<br/>error at saving wlink<br/>')
	
# class profile_images(models.Model):
# 	CHOOSEN_CHOICES = (
# 		(u'T',u'Choosen'),
# 		(u'F',u'UnChoose'),
# 		(u'D',u'Disable'),
# 		)
# 	imagefile=models.ImageField(upload_to=get_file_path)
# 	user_id=models.IntegerField(default=1)
# 	user_level=models.IntegerField(default=9)
# 	date=models.DateField(auto_now=True)
# 	now=datetime.datetime.now()
# 	person_id=models.ForeignKey(movie_persons)
# 	directory_string_var = 'person_image/%s/%s/%s/'%(now.year,now.month,now.day)
# 	image_choosen=models.CharField(max_length=2,choices=CHOOSEN_CHOICES)
	
	for url in image_urls:
		print url
		new_image=profile_images()
		try:
			img_temp = NamedTemporaryFile(delete=True)
			opener = urllib2.build_opener()
			opener.addheaders = [('User-agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:15.0) Gecko/20120427 Firefox/15.0a1')]
			img_temp.write(opener.open(url).read())
			img_temp.flush()
			new_image.user_id=wiki_user_id
			new_image.image_choosen='T'
			new_image.person_id=new_person
			new_image.imagefile.save('.jpg',File(img_temp))
			output.append('image <img height=100px src=\''+url+'\'/><br/>')
		except Exception, e:
			print "error"
			print e
	return HttpResponse(output)