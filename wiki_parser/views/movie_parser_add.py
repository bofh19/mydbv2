from django.core.files import File
from django.core.files.temp import NamedTemporaryFile



from django.http import HttpResponse
import urllib2
import json
import re
from wiki_parser.models import movie_wlinks
from wiki_parser.models import person_wlinks
from django.contrib.auth.models import *

from django.shortcuts import render_to_response
from django.template import RequestContext
from movie_persons.models import movie_persons
from images.models import profile_images
from movies.models import movies
import random

from bs4 import BeautifulSoup
from bs4 import UnicodeDammit

from professions.models import professions

from django.contrib.auth.models import User
from django.db.models import Q

def movie_parser_add(request):
	outputx={}
	if request.user.is_staff:
		pass
	else:
		output = "user not staff"
		return render_to_response('wiki_parser/templates/movie_parser_add.html',
								{
									'output':output,
								},context_instance=RequestContext(request)
								)
	output = ''
	try:
		wlink=request.GET['wlink']
	except Exception, e:
		output='no link in url'
		return render_to_response('wiki_parser/templates/movie_parser_add.html',
								{
									'output':output,
								},context_instance=RequestContext(request)
								)

	url = """http://en.wikipedia.org/w/api.php?action=parse&page="""+str(wlink)+"""&format=json&prop=text"""#
	url = re.sub(' ','_',url)
	print url
	f = urllib2.urlopen(url)
	a=json.loads(f.read())	
	outputx = a['parse']['text']['*']
	soup = BeautifulSoup(outputx)
	output = soup.find_all('table')
	tables=[]
	for each_table in output:
		tablex=[]
		for each_row in each_table.find_all('tr'):
			tablex_row={}
			tablex_head=[]
			tablex_cols=[]
			tablex_head_tags=[]
			for each_head in each_row.find_all('th'):
				dammit = UnicodeDammit(each_head.get_text())
				htext=dammit.unicode_markup
				htext=re.sub(r'[^\w]', ' ', htext)
				htext = re.sub('\[\d+\]','',htext)
				tablex_head.append(htext)
			for each_col in each_row.find_all('td'):
				dammit = UnicodeDammit(each_col.get_text())
				ctext = dammit.unicode_markup
				ctext = re.sub('\[\d+\]','',ctext)
				tablex_cols.append(ctext)
			
			tablex_row['head']=tablex_head
			tablex_row['head_tags']=tablex_head_tags
			tablex_row['cols']=tablex_cols
			tablex.append(tablex_row)
		tables.append(tablex)
	output = soup.find_all('p')
	summary = output[0].get_text()
	dammit = UnicodeDammit(summary)
	summary = dammit.unicode_markup
	summary  = '<p> '+ summary+' </p>'
	summary = re.sub('\[\d+\]','',summary)
	output = output[1:]
	ptext='<p>'
	for out in output:
		ptxt=out.get_text()
		ptxt = re.sub('\[\d+\]','',ptxt)
		dammit = UnicodeDammit(ptxt)
		pxt=dammit.unicode_markup
		ptext=ptext+ptxt+' </p><p> '
	
	ptext=ptext+'</p>'
	jpg_images=[]
	imgz=soup.find_all('img')
	for img in imgz:
		a=re.search('.jpg',img['src'])
		if a:
			jpg_images.append('http:'+str(img['src']))
		a=re.search('.JPG',img['src'])
		if a:
			jpg_images.append('http:'+str(img['src']))

	return render_to_response('wiki_parser/templates/movie_parser_add.html',
								{
									'output':output,
									'results':outputx,
									'wlink':str(wlink),
									'ptext':ptext,
									'images':jpg_images,
									'tables':tables,
									'plot':summary,
								},context_instance=RequestContext(request)
							 )

from django.contrib.auth.models import User
import json
from wiki_parser.models import movie_wlinks
from images.models import banner_images
from images.models import background_images


def movie_parser_adding(request):
	if request.user.is_staff:
		pass
	else:
		output = "user not staff"
		return HttpResponse('user not staff')
	output = []
	out={}
	image_urls=[]
	outputx=''
	post = request.POST
	try:
		wiki_user=User.objects.get(username='wiki_parser')
		wiki_user_id=int(wiki_user.id)
	except Exception, e:
		return HttpResponse('user: wiki_parser does not exist. create a new user with username: wiki_parser to continue')
	x=0
	if len(request.POST)>4:
		while (x< (len(request.POST)-3)):
			try:
				this_url=post['imagefile'+str(x)]
				if this_url != '':
					image_urls.append(this_url)
			except Exception, e:
				pass
			
			x=x+1
	try:
		this_back_url=post['back_imagefile']
		if this_back_url != '':
			background_image_url = this_back_url
	except Exception, e:
		pass
	jsondumps = json.dumps(request.POST)
	movie_wlink = re.sub('_',' ',post['movie_wlink'])
	movie_wlink_text = re.sub('\.',' ',movie_wlink)
	movie_wlink_text = re.sub('\(film\)',' ',movie_wlink_text)

	new_movie = movies()
	new_movie.movie_plot = post['movie_plot']
	new_movie.movie_info = post['movie_info']
	new_movie.movie_summary = post['movie_summary']
	new_movie.movie_name = movie_wlink_text
	new_movie.movie_year=post['movie_date']
	new_movie.save()

	output.append('movie saved')

	new_movie_wlinks=movie_wlinks()
	new_movie_wlinks.movie_id=new_movie
	new_movie_wlinks.wtitle = post['movie_wlink']
	new_movie_wlinks.save()

	output.append('movie wlink saved')

	for url in image_urls:
		new_image=banner_images()
		try:
			img_temp = NamedTemporaryFile(delete=True)
			opener = urllib2.build_opener()
			opener.addheaders = [('User-agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:15.0) Gecko/20120427 Firefox/15.0a1')]
			img_temp.write(opener.open(url).read())
			img_temp.flush()
			new_image.user_id=wiki_user_id
			new_image.user_level = 9
			new_image.image_choosen='T'
			new_image.movie_id=new_movie
			new_image.imagefile.save('.jpg',File(img_temp))
			output.append('image <img height=100px src=\''+url+'\'/><br/>')
		except Exception, e:
			print "error"
			print e
	
	if background_image_url:
		new_image=background_images()
		url = background_image_url
		img_temp = NamedTemporaryFile(delete=True)
		opener = urllib2.build_opener()
		opener.addheaders = [('User-agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:15.0) Gecko/20120427 Firefox/15.0a1')]
		img_temp.write(opener.open(url).read())
		img_temp.flush()
		new_image.user_id=wiki_user_id
		new_image.user_level = 9
		new_image.image_choosen='T'
		new_image.movie_id=new_movie
		new_image.imagefile.save('.jpg',File(img_temp))
		output.append('<hr/>background image <img height=100px src=\''+url+'\'/><br/>')

	new_movie_id = int(new_movie.id)
	redirection = """	<script type="text/javascript">
							function myFunction()
							{
							setTimeout(function(){
										window.location.replace(\'/parse/movie/cast/"""+str(new_movie_id)+"""/\')
								    },3000);
							}
							myFunction();
						</script>
						"""
	output.append(redirection)
	return HttpResponse(output)


