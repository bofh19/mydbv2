from movie_characters.models import movie_characters
from django.contrib import admin

class movie_characterAdmin(admin.ModelAdmin):
	model = movie_characters
	
	search_fields = ['character_name']

admin.site.register(movie_characters,movie_characterAdmin)