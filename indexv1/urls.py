from django.conf.urls import patterns, include, url

urlpatterns = patterns('indexv1.views',

	url(r'^api/latestdb/$','latestdb',name='api_latestdb'), 
	url(r'^api/upcomming/$','upcomming',name='api_upcomming'), 
	url(r'^api/intheaters/$','intheaters',name='api_intheaters'), 

	url(r'^latest-trailers/','latest_trailers'),

	
	url(r'^$','v1',name='v1'),
	url(r'^i/latestdb/$','i_latestdb',name='i_latestdb'),
	url(r'^i/footer/$','i_footer',name='i_footer'),

	 url(r'^api/m/(?P<movie_id>\d+)/$','m_info',name='m_info'),
	 url(r'^api/m/cast/(?P<movie_id>\d+)/$','m_info_cast',name='m_info_cast'),
	 url(r'^api/m/images/(?P<movie_id>\d+)/$','movie_images_all',name='movie_images_all'),
	 
	 # url(r'^api/m/backimages/all/$','background_images_all',name='background_images_all'),
	 
	 url(r'^api/m/backimages/(?P<movie_id>\d+)/$','background_images_movie',name='background_images_movie'),


	 url(r'^api/p/(?P<person_id>\d+)/$','p_info',name='p_info'),
	 url(r'^api/p/images/(?P<person_id>\d+)/$','person_images_all',name='person_images_all'),

	 url(r'^api/searching/all/$','searching_all',name='searching_all'),
	 url(r'^api/search/all/$','search_all',name='search_all'),

	 #movie & person frames to share
	 url(r'^frame/m/(?P<movie_id>\d+)/$','movie_detail_frame',name='movie_detail_frame'),

	 #images 
	  url(r'^image/list/$','listview',name='listviewv2'),
    # url(r'^image/listurl/$','listview_url',name='listview_url'),
    url(r'^image/(?P<image_id>\d+)/$','image_view'),
    url(r'^image/(?P<image_id>\d+)/ajaxtaging/$','image_tag'),




	 #all actors
	 url(r'^api/allpersons/$','persons_list_all',name='persons_list_all'), 

	 # test ones
	 url(r'^api/music/ls/$','music_list_local_dir',name='music_list_local_dir'),

	 
)


