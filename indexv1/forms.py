from django import forms
from movies.models import movies
import datetime

class DocumentForm(forms.Form):
	imagefile=forms.FileField(label='select an image',help_text='max 42 megabytes')
#	user_id=forms.IntegerField()
#	user_level=forms.IntegerField()
	image_desc=forms.CharField(max_length=100,required=True)
	movie=forms.CharField(max_length=100,required=True,initial="None",widget=forms.TextInput(attrs={'id' : 'movie_form'}))
	TYPE_CHOICES=(
		(u'Movie',u'Movie'),
		(u'Person',u'Person'),
		(u'function',u'function'),
		(u'working-still',u'working-still'),
		)
	image_type=forms.ChoiceField(choices=TYPE_CHOICES,required=True,initial=None)
	
	def clean_movie(self):
		movie_name = self.cleaned_data['movie']
		test_movie=movies()
		try:
			test_movie=movies.objects.get(movie_name=movie_name)
			return test_movie
		except test_movie.DoesNotExist:
			raise forms.ValidationError(u'%s Movie Does Not Exist' % test_movie)
"""
class DocumentFormUrl(forms.Form):
	imagepath=forms.FileField(label='select an image',help_text='max 42 megabytes')
#	user_id=forms.IntegerField()
#	user_level=forms.IntegerField()
	image_desc=forms.CharField(max_length=100,required=True)
	movie=forms.CharField(max_length=100,required=True,initial="None",widget=forms.TextInput(attrs={'id' : 'movie_form'}))
	TYPE_CHOICES=(
		(u'Movie',u'Movie'),
		(u'Person',u'Person'),
		(u'function',u'function'),
		(u'working-still',u'working-still'),
		)
	image_type=forms.ChoiceField(choices=TYPE_CHOICES,required=True,initial=None)
	
	def clean_movie(self):
		movie_name = self.cleaned_data['movie']
		test_movie=movies()
		try:
			test_movie=movies.objects.get(movie_name=movie_name)
			return test_movie
		except test_movie.DoesNotExist:
			raise forms.ValidationError(u'%s Movie Does Not Exist' % test_movie)

			"""