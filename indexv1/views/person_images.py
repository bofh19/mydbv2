# Create your views here.
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect,HttpResponse
from images.models import images_tags
from images.models import images
from images.models import image_seq
from images.forms import *
from django.core.urlresolvers import reverse
from movies.models import movies

from movie_persons.models import movie_persons
from images.models import profile_images


mimetype='application/json'

import random
import json
import re

from datetime import *
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.csrf import requires_csrf_token
from django.views.decorators.csrf import ensure_csrf_cookie

from sorl.thumbnail import get_thumbnail
MOVIE_PERSON_THUMBS_SIZE = '80x80'
MOVIE_BANNER_THUMBS_SIZE = '100x120'
MOVIE_BANNER_SIZE = '200x300'


def person_images_all(request,person_id):
	result={}
	result['images']=[]
	images_all=[]
	try:
		this_person=movie_persons.objects.get(id=person_id)
		name=this_person
	except Exception, e:
		this_person=''
		this_person_images=''

	try:
		this_person_profile_imagez=profile_images.objects.filter(person_id=person_id)
		# this_person_profile_images=[]
		for x in this_person_profile_imagez:
			current_image = {}
			current_image['actual'] = x.imagefile.url
			try:
				current_image['small'] = get_thumbnail(x.imagefile,MOVIE_BANNER_THUMBS_SIZE,crop='center').url
			except Exception, e:
				current_image['small'] = get_thumbnail('template_images/no_image.jpg', MOVIE_BANNER_THUMBS_SIZE, crop='top').url
			images_all.append(current_image)
			# this_person_profile_images.append(x)
	except Exception, e:
		pass
	
	try:
		this_person_imagez=images_tags.objects.filter(person_id=person_id)
		if len(this_person_imagez)>1:
			this_person_imagez=this_person_imagez[::-1]
		# this_person_images=[]

		for a in this_person_imagez:
			current_image = {}
			try:
				img = images.objects.get(id=a.image_id.id)
			except Exception, e:
				img=''
				break
			current_image['actual'] = img.imagefile.url
			try:
				current_image['small'] = get_thumbnail(img.imagefile, MOVIE_BANNER_THUMBS_SIZE, crop='center').url
			except:
				current_image['small'] = get_thumbnail('template_images/no_image.jpg', MOVIE_BANNER_THUMBS_SIZE, crop='top').url
			images_all.append(current_image)
			
			# this_person_images.append(images.objects.get(id=a.image_id.id))
	except Exception, e:
		pass
	
	result['images']=images_all
	result_json = json.dumps(result)	
	return HttpResponse(result_json,mimetype)