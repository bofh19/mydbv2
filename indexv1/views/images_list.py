# Create your views here.
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect,HttpResponse
from images.models import images_tags
from images.models import images
from images.models import image_seq
from django.core.urlresolvers import reverse
from movies.models import movies
from movie_persons.models import movie_persons
from indexv1.forms import *
MEDIA_SERVER_URL="http://127.0.0.1:8000"
def listview(request):
	if request.method=='POST':
		if request.user.is_authenticated():
			username=request.user.username
		else:
			return HttpResponseRedirect('/accounts/login/?next=/image/list/')
		form=DocumentForm(request.POST,request.FILES)

		if form.is_valid():
			newdoc=images(imagefile=request.FILES['imagefile'])
			newdoc.user_id=request.user
			newdoc.user_level=9
			newdoc.image_desc=(request.POST['image_desc'])
			newdoc.tag_enabled=True

			try:
				movie_id=movies.objects.get(movie_name__iexact=request.POST['movie'])
			except:
				form=DocumentForm()
				return HttpResponse('cant find movie')
			newdoc.save()
			newdoc_seq=image_seq()
			newdoc_seq.image_id=newdoc
			newdoc_seq.movie_id=movie_id
			newdoc_image_tags=images_tags()
			newdoc_image_tags.image_id=newdoc
			newdoc_image_tags.movie_id=movie_id
			newdoc_image_tags.image_type=request.POST['image_type']
			newdoc_image_tags.user_id=request.user
			print request.POST['image_type']
			try:
				newdoc_image_tags.save()
			except:
				newdoc.delete()
				return HttpResponse("unable to save tag")
			try:
				newdoc_seq.save()
			except:
				newdoc.delete()
				newdoc_image_tags.delete()
				return HttpResponse("unable to savge seq")

			return HttpResponseRedirect(reverse('images.views.listview'))
	else:
		try:
			if request.GET['movie']:
				form = DocumentForm(initial={'movie':request.GET['movie']})
			else:
				form = DocumentForm()
		except Exception, e:
			form = DocumentForm()
	images_list=images.objects.all()[::-1]
	images_list=images_list
	return render_to_response(
		'indexv1/templates/images_list.html',
		{'documents':images_list,
		 'form':form
		},
		context_instance=RequestContext(request)
)

def listview_url(request):
	if request.method=='POST':
		if request.user.is_authenticated():
			username=request.user.username
		else:
			return HttpResponseRedirect('/accounts/login/?next=/image/list/')
		form=DocumentForm(request.POST,request.FILES)

		if form.is_valid():
			newdoc=images(imagefile=request.FILES['imagefile'])
			newdoc.user_id=request.user
			newdoc.user_level=9
			newdoc.image_desc=(request.POST['image_desc'])
			newdoc.tag_enabled=True

			try:
				movie_id=movies.objects.get(movie_name__iexact=request.POST['movie'])
			except:
				form=DocumentForm()
				return HttpResponse('cant find movie')
			newdoc.save()
			newdoc_seq=image_seq()
			newdoc_seq.image_id=newdoc
			newdoc_seq.movie_id=movie_id
			newdoc_image_tags=images_tags()
			newdoc_image_tags.image_id=newdoc
			newdoc_image_tags.movie_id=movie_id
			newdoc_image_tags.image_type=request.POST['image_type']
			newdoc_image_tags.user_id=request.user
			print request.POST['image_type']
			try:
				newdoc_image_tags.save()
			except:
				newdoc.delete()
				return HttpResponse("unable to save tag")
			try:
				newdoc_seq.save()
			except:
				newdoc.delete()
				newdoc_image_tags.delete()
				return HttpResponse("unable to savge seq")

			return HttpResponseRedirect(reverse('images.views.listview_url'))
	else:
		form = DocumentForm()

	images_list=images.objects.all()[::-1]
	images_list=images_list[:10]
	return render_to_response(
		'indexv1/templates/images_list.html',
		{'documents':images_list,
		 'form':form
		},
		context_instance=RequestContext(request)
)

def image_view(request,image_id):
	try:
		this_image=images.objects.get(id=image_id)
		
	except:
		this_image=''

	tags_persons=[]
	tags_type=[]
	tags_movie=[]
	try:
		all_tags=images_tags.objects.filter(image_id=image_id)
	except:
		all_tags=''
	try:
		total_images=len(images.objects.all())
		#print total_images
		try:
			next_image=images.objects.filter(id__gt=image_id)[0]
		except Exception, e:
		#	print e 
			next_image=''
		try:
			previous_image=images.objects.filter(id__lt=image_id)[::-1][0]
		except Exception, e:
		#	print e
			previous_image=''
	except Exception, e:
		#print "Exception "
		#print e
		next_image=''
		previous_image=''
	for a in all_tags:
		try:
			tags_persons.append(a.person_id)
			if (a.image_type==None):
				pass
			else:
				tags_type.append(a.image_type)
			tags_type=list(set(tags_type))
			tags_movie.append(a.movie_id)
			tags_movie=list(set(tags_movie))
			tags_characters.append(a.character_id)
			tags_characters=list(set(tags_characters))
		except:
			pass
	return render_to_response(
		'indexv1/templates/image_view.html',
		{'image':this_image,
		 'tags_movie':tags_movie,
		 'tags_type':tags_type,
		 'tags_persons':tags_persons,
		 'next':next_image,
		 'previous':previous_image,
		 'total_images':total_images,
		},
		context_instance=RequestContext(request)
)

def image_tag(request,image_id):
	try:
		image=images.objects.get(id=image_id)
	except:
		return HttpResponse("cant find image..what are you trying to do??")

	if request.user.is_authenticated(): 							# 1
		username=request.user.username
	else:
		return HttpResponse("""login before to tag <a href=/movie/index/>login here</a>""")
	if(image.tag_enabled):
		pass
	else:
		return HttpResponse("tagging not allowed for this image at this time")
	new_image_tag=images_tags()
	try:
		x=request.POST['person_name']
	except:
		return HttpResponse("cant find this person please dont add anything extra to name") 
	try:
		person_id=movie_persons.objects.get(person_name__iexact=request.POST['person_name'])
	except:
		return HttpResponse("cant find this "+request.POST['person_name']+" please dont add anything extra to name") 

	new_image_tag.user_id=request.user
	new_image_tag.image_id=image
	new_image_tag.person_id=person_id
	try:
		new_image_tag.save()
	except:
		return HttpResponse('error while saving this tag. already exists or tagging not allowed')
	return HttpResponse("Tag Saved Reload the page to see it")
