import os
import random
from settings import MEDIA_ROOT
import json
from django.http import HttpResponse
def music_list_local_dir(request):
	path=MEDIA_ROOT+'/music'
	mimetype='application/json'
	structure=[]
	result = {}
	result['ls'] = []
	for name in os.listdir(path):
		this_dir={}
		if os.path.isdir(os.path.join(path,name)):
			this_dir['dirname']=name
			this_dir_files={}
			this_dir_file_mp3=[]
			this_dir_file_jpg=[]
			for each_file in os.listdir(os.path.join(path,name)):
				if os.path.isfile(os.path.join(path,name,each_file)):					
					filename,fileext=os.path.splitext(os.path.join(path,name,each_file))
					if fileext=='.mp3' or fileext=='.MP3' or fileext=='.wav' or fileext=='.WAV' or fileext=='.ogg':
						this_dir_file_mp3.append(each_file)
					elif fileext=='.jpg' or fileext=='.png' or fileext=='.jpeg'or fileext=='.JPG'or fileext=='.PNG'or fileext=='.GIF'or fileext=='.gif':
						this_dir_file_jpg.append(each_file)
					
			this_dir_files['mp3']=this_dir_file_mp3
			this_dir_files['jpg']=this_dir_file_jpg

			this_dir['files']=this_dir_files
		structure.append(this_dir)
		result['ls']=structure
		result_json = json.dumps(result)
	return HttpResponse(result_json,mimetype)
