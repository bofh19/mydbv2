from django.http import HttpResponse
from urlparse import urlparse
from django.shortcuts import render_to_response
from django.template import RequestContext
from images.models import background_images
from movie_music.models import *
from movies.models import movies
from movies.models import movie_theater_status
from movie_trailers.models import movie_trailers
from images.models import banner_images
from movie_genres.models import movie_genres
from genres.models import genres
from movie_persons.models import movie_persons
from movie_casting.models import movie_casting
from professions.models import professions
from movies.models import movie_theater_status
from images.models import profile_images
from movie_characters.models import movie_characters

import random
import json
import re

from datetime import *
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.csrf import requires_csrf_token
from django.views.decorators.csrf import ensure_csrf_cookie

# movie_id=7
# url= "http://127.0.0.1:8000/api/mif/"+movie_id+"/"
# $.getJSON(url,function(data){console.log(data)})


from sorl.thumbnail import get_thumbnail
MOVIE_PERSON_THUMBS_SIZE = '80x80'
MOVIE_BANNER_THUMBS_SIZE = '80x80'
MOVIE_BANNER_SIZE = '200x300'




@ensure_csrf_cookie
@requires_csrf_token
@csrf_protect
def v1(request):
	return render_to_response('indexv1/templates/v1.html',
									{
									},context_instance=RequestContext(request)
								)

def i_footer(request):
	return render_to_response('indexv1/templates/i_footer.html',
									{
									},context_instance=RequestContext(request)
								)

def i_latestdb(request):
	return render_to_response('indexv1/templates/i_latestdb.html',
									{
									},context_instance=RequestContext(request)
								)




