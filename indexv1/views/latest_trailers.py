# Create your views here.
from django.shortcuts import render_to_response
from django.template import RequestContext

from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse

from movies.models import movies
from movie_trailers.models import movie_trailers

import random

from django.http import Http404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


def latest_trailers(request):
	
	items_per_page=10
	if request.user.is_authenticated():
		last_login = request.session.get('social_auth_last_login_backend')
	else:
		last_login=''
	
	try:
		if request.GET['p']:
			page_number=int(request.GET['p'])
		else:
			page_number=1
	except Exception, e:
		page_number=1
	page=page_number
	this_trailers=movie_trailers.objects.all()[::-1]
	paginator = Paginator(this_trailers,items_per_page)

	try:
		latest_trailers = paginator.page(page)
	except PageNotAnInteger:
		latest_trailers = paginator.page(1)
	except EmptyPage:
		latest_trailers = paginator.page(paginator.num_pages)

	#
	#latest_trailers=this_trailers[:10]
	return render_to_response('index/templates/latest_trailers.html',
								{
								'last_login':last_login,
								'latest_trailers':latest_trailers,
								'page_list':latest_trailers,
							
								},context_instance=RequestContext(request)
							)
def sortwithdays(a):
	x=len(a)
	i=0
	while(x>1):
		i=0
		while(i<(x-1)):
			if a[i]['days_left']>a[i+1]['days_left']:
				b=a[i]
				a[i]=a[i+1]
				a[i+1]=b
			i=i+1
		x=x-1
	return a