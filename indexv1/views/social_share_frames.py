from django.shortcuts import render_to_response
from django.shortcuts import get_object_or_404
from movies.models import movies
from django.template import RequestContext
from movie_genres.models import movie_genres
from genres.models import genres
from user_votes.models import user_votes
from movie_ratings.models import movie_ratings
from images.models import image_seq
from images.models import images
from images.models import profile_images
from movie_casting.models import movie_casting
from movie_persons.models import movie_persons
from movie_characters.models import movie_characters
from images.models import background_images
from images.models import banner_images
from movie_ratings.models import rating_cat
from movie_music.models import movie_music, song_persons_info, song_video_info
from movie_music.models import song_links
from professions.models import professions
import json, random
from movies.models import movie_theater_status
from wiki_parser.models import movie_wlinks

MIN_NUM_VOTES = 1
MEDIA_SERVER_URL="http://127.0.0.1:8000"
def sortpowh(a):
	x=len(a)
	i=0
	#print x
	while(x>1):
		#print x
		i=0
		while(i<(x-1)):
			if a[i]['powh']>a[i+1]['powh']:
				b=a[i]
				a[i]=a[i+1]
				a[i+1]=b
			i=i+1
		x=x-1
	a.reverse()
	return a
def getpowh70(a):
	z=[]
	for x in a:
		if x['powh']>=70:
			z.append(x)
	return z	
		
from datetime import *
def movie_detail_frame(request,movie_id):
	try:
		movie_name=get_object_or_404(movies,id=movie_id)
	except UnboundLocalError:
		movie_name="Does Not Exist"
		return render_to_response('indexv1/templates/movie_frame_share.html',
								{
									'name':movie_name,
								},context_instance=RequestContext(request)
							)
	except movies.DoesNotExist:
		movie_name="Does Not Exist"
		return render_to_response('indexv1/templates/movie_frame_share.html',
								{
									'name':movie_name,
								},context_instance=RequestContext(request)
						)


	try:
		
		this_movie_backimages=background_images.objects.filter(movie_id=movie_id)
		this_movie_backimage=random.choice(this_movie_backimages)
	except background_images.DoesNotExist:
		this_movie_backimages=[]
		this_movie_backimage=''
	except IndexError:
		this_movie_backimages=[]
		this_movie_backimage=''
	except Exception,e:
		this_movie_backimages=[]
		this_movie_backimage=''
	
	movie=movies.objects.get(id=movie_id)

	movie_json={}
	movie_json['id']=movie

	try:
		genres=movie_genres.objects.filter(movie_id=movie.id)
	except UnboundLocalError:
		genres="Does Not Exist"
	except movie_genres.DoesNotExist:
		genres="Does Not Exist"

	movie_json['genres']=genres

	try:
		movie_per=[]
		movie_pers=[]
		try:
			prof_id=professions.objects.get(profession_name='Hero')
		except Exception, e:
			pass
		try:
			this_movie_persons=movie_casting.objects.filter(movie_id=movie.id,profession_id=prof_id.id)
			#print this_movie_persons
		except Exception, e:
			pass
		for person in this_movie_persons:
			movie_per.append(movie_persons.objects.get(id=person.movie_persons_id.id))

		prof_id=professions.objects.get(profession_name='Heroine')
		this_movie_persons=(movie_casting.objects.filter(movie_id=movie.id,profession_id=prof_id.id))
		for person in this_movie_persons:
			movie_per.append(movie_persons.objects.get(id=person.movie_persons_id.id))

		prof_id=professions.objects.get(profession_name='Director')
		this_movie_persons=(movie_casting.objects.filter(movie_id=movie.id,profession_id=prof_id.id))
		for person in this_movie_persons:
			movie_per.append(movie_persons.objects.get(id=person.movie_persons_id.id))

		prof_id=professions.objects.get(profession_name='Music Director')
		this_movie_persons=(movie_casting.objects.filter(movie_id=movie.id,profession_id=prof_id.id))
		for person in this_movie_persons:
			movie_per.append(movie_persons.objects.get(id=person.movie_persons_id.id))

		prof_id=professions.objects.get(profession_name='Producer')
		this_movie_persons=(movie_casting.objects.filter(movie_id=movie.id,profession_id=prof_id.id))
		 		
		for person in this_movie_persons:
			movie_per.append(movie_persons.objects.get(id=person.movie_persons_id.id))

		movie_per=list(set(movie_per))
		movie_json['persons'] = movie_per
	except Exception, e:

		movie_json['persons']=''

	try:
		this_movie_banner_images=banner_images.objects.filter(movie_id=movie_id)
		this_movie_banner_img=random.choice(this_movie_banner_images)
		this_movie_banner_image=this_movie_banner_img
	except banner_images.DoesNotExist:
		this_movie_banner_image=''
	except:
		this_movie_banner_image=''

	try:
		this_movie_theater_status=movie_theater_status.objects.get(movie_id=movie_id).release_status
	except Exception, e:
		this_movie_theater_status=''
	
	now=date.today()
	diff=movie.movie_year-now
	days_left=diff.days
	return render_to_response('indexv1/templates/movie_frame_share.html',
								{
									'movie_id':movie_id,
									'name':movie_name,
									'background_image':this_movie_backimage,
									'movie':movie_json,
									'banner_image':this_movie_banner_image,
									'theater_status':this_movie_theater_status,
									'days_left':days_left,
								},context_instance=RequestContext(request)
							)

