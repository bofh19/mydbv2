from django.http import HttpResponse
from urlparse import urlparse
from django.shortcuts import render_to_response
from django.template import RequestContext
from images.models import background_images
from movie_music.models import *
from movies.models import movies
from movies.models import movie_theater_status
from movies.models import movie_dates
from movie_trailers.models import movie_trailers
from images.models import banner_images
from movie_genres.models import movie_genres
from genres.models import genres
from movie_persons.models import movie_persons
from movie_casting.models import movie_casting
from professions.models import professions
from movies.models import movie_theater_status
from images.models import profile_images
from movie_characters.models import movie_characters

import random
import json
import re

from datetime import *
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.csrf import requires_csrf_token
from django.views.decorators.csrf import ensure_csrf_cookie

# movie_id=7
# url= "http://127.0.0.1:8000/api/mif/"+movie_id+"/"
# $.getJSON(url,function(data){console.log(data)})


from sorl.thumbnail import get_thumbnail
MOVIE_PERSON_THUMBS_SIZE = '80x80'
MOVIE_BANNER_THUMBS_SIZE = '100x120'
MOVIE_BANNER_SIZE = '220x320'

from django.db.models import Q

def sortpowh(a):
	x=len(a)
	i=0
	#print x
	while(x>1):
		#print x
		i=0
		while(i<(x-1)):
			if a[i]['powh']>a[i+1]['powh']:
				b=a[i]
				a[i]=a[i+1]
				a[i+1]=b
			i=i+1
		x=x-1
	a.reverse()
	return a


# @ensure_csrf_cookie
# @requires_csrf_token
# @csrf_protect	
def m_info(request,movie_id):
	# if request.is_ajax():
		result = {}
		mimetype='application/json'

		movie_json={}

		try:
			movie=movies.objects.get(id=movie_id)
		except Exception, e:
			result_json='none'
			return HttpResponse(result_json,mimetype)

		movie_json['id']=movie.id
		movie_json['name']=movie.movie_name
		genre_json=[]
		try:
			genres_all=movie_genres.objects.filter(movie_id=movie.id)
		except UnboundLocalError:
			genres_all=''
		except movie_genres.DoesNotExist:
			genres_all=''
		genres_all = list(set(genres_all))
		
		for genre in genres_all:
			current_genre = {}
			current_genre['id'] = genre.genre_id.id
			current_genre['name'] = genre.genre_id.genre_name
			genre_json.append(current_genre)

		movie_json['genres']=genre_json

		now=date.today()
		diff=now-movie.movie_year
		movie_json['days_left']=diff.days*(-1)

		date_json={}

		date_json['year']=movie.movie_year.year
		date_json['day']=movie.movie_year.day
		date_json['month']=movie.movie_year.month
		date_json['weekday']=movie.movie_year.weekday()
		if movie.movie_year.year == 1800:
			date_json['year_condition'] = False
		else:
			date_json['year_condition'] = True
		movie_json['date']=date_json
		try:
			movie_json['status']=movie_theater_status.objects.get(movie_id=movie_id).release_status
		except Exception, e:
			movie_json['status']=''

		if movie.movie_info:
			movie_json['info']=movie.movie_info
		else:
			movie_json['info']=''
		movie_json['all_dates'] = []
		try:
			movie_dates_all = movie_dates.objects.filter(movie_id=movie_id)
			for each_date in movie_dates_all:
				this_date_json = {}
				this_date_json['day'] = each_date.movie_date.day
				this_date_json['month'] = each_date.movie_date.month
				this_date_json['year'] = each_date.movie_date.year
				this_date_json['desc'] = each_date.desc
				movie_json['all_dates'].append(this_date_json)
		except Exception, e:
			print e

		movie_json['plot']=movie.movie_plot
		movie_json['summary']=movie.movie_summary


##################### persons
		try:
			this_movie_persons=movie_casting.objects.filter(movie_id=movie_id)
		
			persons_id=[]
			#print "x"
			for this_movie_person in this_movie_persons:
				#print this_movie_person.movie_persons_id.id
				persons_id.append(this_movie_person.movie_persons_id.id)		
			persons_id=list(set(persons_id))
		except movie_casting.DoesNotExist:
			pass
		result=[]
		#print persons_id
	
		for person_id in persons_id:
		#	print person_id
			person_json={}
			profession_json=[]
			person_json['id']=person_id
			person_json['powh']=0
			try:
				person_json['name']=movie_persons.objects.get(id=person_id).person_name
				#print person_json['name']
			except movie_persons.DoesNotExist:
				person_json['name']="Not avilable error"+str(person_id)

			try:
				this_person_profile_image = random.choice(profile_images.objects.filter(person_id=person_id,image_choosen='T'))
				person_json['profile_image']=this_person_profile_image.imagefile.url
				person_json['profile_image_small'] = get_thumbnail(this_person_profile_image.imagefile, MOVIE_PERSON_THUMBS_SIZE, crop='top').url
			except Exception, e:
				print e
				person_json['profile_image']='/media/template_images/no_image.jpg'
				person_json['profile_image_small'] = get_thumbnail('template_images/no_image.jpg', MOVIE_PERSON_THUMBS_SIZE, crop='top').url
			try:
				person_json['character'] = movie_characters.objects.get(movie_persons_id=person_id,movie_id=movie_id).character_name			
			except:
				person_json['character']=''	
			try:
				person_json['character_id'] = movie_characters.objects.get(movie_persons_id=person_id,movie_id=movie_id).id
			except:
				person_json['character_id']=''
			for z in movie_casting.objects.filter(movie_persons_id=person_id,movie_id=movie_id):
				person_professions={}
				person_professions['id']=z.profession_id.id
				person_professions['name']=z.profession_id.profession_name
				person_professions['catagory'] = z.profession_id.profession_catagory
				person_json['powh']=person_json['powh']+z.profession_id.profession_level
				profession_json.append((person_professions))
			person_json['professions']=profession_json
			result.append(person_json)
		#asdf=json.dumps(result)
		#print asdf
		result=sortpowh(result)
		persons=result		

		movie_json['persons']=persons

		try:
			this_trailers=movie_trailers.objects.filter(movie_id=movie.id).values('youtube_code')[::-1]
			if this_trailers:
				movie_json['trailers']=this_trailers
			else:
				movie_json['trailers']=''
		except Exception, e:
			movie_json['trailer']=''

		try:
			this_movie_banner_images=banner_images.objects.filter(movie_id=movie.id)
			this_movie_banner_image=random.choice(this_movie_banner_images)
			movie_json['banner_image']=this_movie_banner_image.imagefile.url
			im = get_thumbnail(this_movie_banner_image.imagefile, MOVIE_BANNER_SIZE, crop='top')
			movie_json['banner_image_small']=im.url
			
		except banner_images.DoesNotExist:
			movie_json['banner_image']='/media/template_images/no_image.jpg'
			movie_json['banner_image_small']=get_thumbnail('template_images/no_image.jpg', MOVIE_BANNER__SIZE, crop='top').url
		except Exception, e:
			movie_json['banner_image']='/media/template_images/no_image.jpg'
			movie_json['banner_image_small']=get_thumbnail('template_images/no_image.jpg', MOVIE_BANNER_SIZE, crop='top').url

		try:
			this_movie_background_images=background_images.objects.filter(movie_id=movie.id)
			this_movie_background_image=random.choice(this_movie_background_images)
			movie_json['background_image']=this_movie_background_image.imagefile.url
			
		except banner_images.DoesNotExist:
			movie_json['background_image']='/media/template_images/no_image.jpg'
		except Exception, e:
			movie_json['background_image']='/media/template_images/no_image.jpg'


		result = movie_json
		result_json=json.dumps(result)
		return HttpResponse(result_json,mimetype)
	# else:
	# 	return HttpResponse('fail')

def sortwithdays(a):
	x=len(a)
	i=0
	while(x>1):
		i=0
		while(i<(x-1)):
			if a[i]['days_left']>a[i+1]['days_left']:
				b=a[i]
				a[i]=a[i+1]
				a[i+1]=b
			i=i+1
		x=x-1
	return a


@ensure_csrf_cookie
@requires_csrf_token
@csrf_protect	
def m_info_cast(request,movie_id):
	# if request.is_ajax():
		result = {}
		mimetype='application/json'

		movie_json={}

		try:
			movie=movies.objects.get(id=movie_id)
		except Exception, e:
			result_json='none'
			return HttpResponse(result_json,mimetype)

		movie_json['id']=movie.id
		movie_json['name']=movie.movie_name
		movie_json['jsid']='m_'+str(movie.id)
		genre_json=[]
		try:
			genres_all=movie_genres.objects.filter(movie_id=movie.id)
		except UnboundLocalError:
			genres_all=''
		except movie_genres.DoesNotExist:
			genres_all=''

		for genre in genres_all:
			genre_json.append(genres.objects.get(id=genre.genre_id.id).genre_name)
			genre_json=list(set(genre_json))

		movie_json['genres']=genre_json

		now=date.today()
		diff=now-movie.movie_year
		movie_json['days_left']=diff.days*(-1)

		date_json={}

		date_json['year']=movie.movie_year.year
		date_json['day']=movie.movie_year.day
		date_json['month']=movie.movie_year.month
		date_json['weekday']=movie.movie_year.weekday()

		movie_json['date']=date_json
		try:
			movie_json['status']=movie_theater_status.objects.get(movie_id=movie_id).release_status
		except Exception, e:
			movie_json['status']=''

		if movie.movie_info:
			movie_json['info']=movie.movie_info
		else:
			movie_json['info']=''
		movie_json['plot']=movie.movie_plot
		movie_json['summary']=movie.movie_summary
		
		try:
			this_movie_persons=movie_casting.objects.filter(movie_id=movie_id)
		
			persons_id=[]
		
			for this_movie_person in this_movie_persons:
				persons_id.append(this_movie_person.movie_persons_id.id)		
			persons_id=list(set(persons_id))
		except movie_casting.DoesNotExist:
			pass
		result=[]
		#print persons_id
		
		"""
		person_json = {}
		person_json['id']
		peson_json['name']
		person_json['profile_image']
		person_json['profile_image_small']
		person_json['character_name']
		person_json['character_id']
		"""

		for person_id in persons_id:
		#	print person_id
			person_json={}
			profession_json=[]
			person_json['id']=person_id
			person_json['powh']=0
			try:
				person_json['name']=movie_persons.objects.get(id=person_id).person_name
			except movie_persons.DoesNotExist:
				person_json['name']="Not avilable error"+str(person_id)
			
			try:
				this_person_profile_image=random.choice(profile_images.objects.filter(person_id=person_id,image_choosen='T'))
				person_json['profile_image'] = this_person_profile_image.imagefile.url
				person_json['profile_image_small'] = get_thumbnail(this_person_profile_image.imagefile, MOVIE_PERSON_THUMBS_SIZE, crop='top').url
			except:
				person_json['profile_image']='/media/template_images/no_image.jpg'
	 			person_json['profile_image_small'] = get_thumbnail('template_images/no_image.jpg', MOVIE_PERSON_THUMBS_SIZE, crop='top').url
			
			try:
				this_person_character = movie_characters.objects.get(movie_persons_id=person_id,movie_id=movie_id)
				person_json['character_id'] = this_person_character.id
				person_json['character_name'] = this_person_character.character_name
			except Exception, e:
				person_json['character_id']=''	
				person_json['character_name']=''	

			for z in movie_casting.objects.filter(movie_persons_id=person_id,movie_id=movie_id):
				person_professions={}
				person_professions['id']=z.profession_id.id
				person_professions['name']=z.profession_id.profession_name
				person_json['powh']=person_json['powh']+z.profession_id.profession_level
				profession_json.append((person_professions))
			person_json['professions']=profession_json
			result.append(person_json)
		#asdf=json.dumps(result)
		#print asdf
		result=sortpowh(result)
		# persons=result
		movie_json['persons']=result

		try:
			this_trailers=movie_trailers.objects.filter(movie_id=movie.id).values('youtube_code')[::-1]
			if this_trailers:
				movie_json['trailers']=this_trailers
			else:
				movie_json['trailers']=''
		except Exception, e:
			movie_json['trailer']=''

		try:
			this_movie_banner_images=banner_images.objects.filter(movie_id=movie.id)
			this_movie_banner_image=random.choice(this_movie_banner_images)
			movie_json['banner_image']=this_movie_banner_image.imagefile.url
			im = get_thumbnail(this_movie_banner_image.imagefile, MOVIE_BANNER_SIZE, crop='top')
			movie_json['banner_image_small']=im.url
			
		except banner_images.DoesNotExist:
			movie_json['banner_image']='/media/template_images/no_image.jpg'
			movie_json['banner_image_small']=get_thumbnail('template_images/no_image.jpg', MOVIE_BANNER__SIZE, crop='top').url
		except Exception, e:
			movie_json['banner_image']='/media/template_images/no_image.jpg'
			movie_json['banner_image_small']=get_thumbnail('template_images/no_image.jpg', MOVIE_BANNER_SIZE, crop='top').url

		result = movie_json
		result_json=json.dumps(result)
		return HttpResponse(result_json,mimetype)
	# else:
	# 	return HttpResponse('fail')


def sortpowh(a):
	x=len(a)
	i=0
	#print x
	while(x>1):
		#print x
		i=0
		while(i<(x-1)):
			if a[i]['powh']>a[i+1]['powh']:
				b=a[i]
				a[i]=a[i+1]
				a[i+1]=b
			i=i+1
		x=x-1
	a.reverse()
	return a