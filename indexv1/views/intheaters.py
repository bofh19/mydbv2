from django.http import HttpResponse
from urlparse import urlparse
from django.shortcuts import render_to_response
from django.template import RequestContext
from images.models import background_images
from movie_music.models import *
from movies.models import movies
from movies.models import movie_theater_status
from movie_trailers.models import movie_trailers
from images.models import banner_images
from movie_genres.models import movie_genres
from genres.models import genres
from movie_persons.models import movie_persons
from movie_casting.models import movie_casting
from professions.models import professions
from movies.models import movie_theater_status
from images.models import profile_images
from movie_characters.models import movie_characters

import random
import json
import re

from datetime import *
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.csrf import requires_csrf_token
from django.views.decorators.csrf import ensure_csrf_cookie

# movie_id=7
# url= "http://127.0.0.1:8000/api/mif/"+movie_id+"/"
# $.getJSON(url,function(data){console.log(data)})


from sorl.thumbnail import get_thumbnail
MOVIE_PERSON_THUMBS_SIZE = '80x80'
MOVIE_BANNER_THUMBS_SIZE = '150x200'
MOVIE_BANNER_SIZE = '200x300'

@ensure_csrf_cookie
@requires_csrf_token
@csrf_protect	
def intheaters(request):
	#if request.is_ajax():
		result = {}
		mimetype='application/json'
		result['movies'] = []
		
		movies_in_theater=movie_theater_status.objects.filter(release_status='T')
		latest_movies_list=[]
		for movie in movies_in_theater:
			latest_movies_list.append(movies.objects.get(id=movie.movie_id.id))
		latest_movies_list=latest_movies_list[::-1]

		
		for movie in latest_movies_list:
			movie_json={}
			movie_json['id']=movie.id
			movie_json['name']=movie.movie_name
			movie_json['plot'] = movie.movie_plot
			genre_json=[]
			try:
				genres_all=movie_genres.objects.filter(movie_id=movie.id)
			except UnboundLocalError:
				genres_all=''
			except movie_genres.DoesNotExist:
				genres_all=''

			genres_all_json = []
			for genre in genres_all:
				genre_json2 = {}
				objx = genres.objects.get(id=genre.genre_id.id)
				genre_json2['id'] = objx.id
				genre_json2['name'] = objx.genre_name
				genres_all_json.append(genre_json2)

			movie_json['genres']=genres_all_json

			now=date.today()
			diff=now-movie.movie_year
			
			movie_json['days_left']=diff.days

			if movie_json['days_left'] > 0 and movie_json['days_left'] < 300:
				movie_json['days_condition'] = True
			else:
				movie_json['days_condition'] = False
			date_json={}

			date_json['year']=movie.movie_year.year
			if date_json['year'] >= 1900:
				date_json['year_known'] = True
			else:
				date_json['year_known'] = False
			date_json['day']=movie.movie_year.day
			date_json['month']=movie.movie_year.month
			date_json['weekday']=movie.movie_year.weekday()

			movie_json['date']=date_json

			try:
				movie_per=[]
				movie_pers=[]

				prof_id=professions.objects.get(profession_name='Hero')
				this_movie_persons=movie_casting.objects.filter(movie_id=movie.id,profession_id=prof_id.id)
				
				movie_pers = []
				for person in this_movie_persons:
					person_json = {}
					person_json['id'] = person.movie_persons_id.id
					person_json['name'] = person.movie_persons_id.person_name
					movie_pers.append(person_json)
				movie_json['heros'] = movie_pers
					
				prof_id=professions.objects.get(profession_name='Heroine')
				this_movie_persons=(movie_casting.objects.filter(movie_id=movie.id,profession_id=prof_id.id))
				
				movie_pers = []
				for person in this_movie_persons:
					person_json = {}
					person_json['id'] = person.movie_persons_id.id
					person_json['name'] = person.movie_persons_id.person_name
					movie_pers.append(person_json)
				movie_json['heroines'] = movie_pers

				prof_id=professions.objects.get(profession_name='Director')
				this_movie_persons=(movie_casting.objects.filter(movie_id=movie.id,profession_id=prof_id.id))
				movie_pers = []
				for person in this_movie_persons:
					person_json = {}
					person_json['id'] = person.movie_persons_id.id
					person_json['name'] = person.movie_persons_id.person_name
					movie_pers.append(person_json)
				movie_json['directors'] = movie_pers

				prof_id=professions.objects.get(profession_name='Music Director')
				this_movie_persons=(movie_casting.objects.filter(movie_id=movie.id,profession_id=prof_id.id))
				movie_pers = []
				for person in this_movie_persons:
					person_json = {}
					person_json['id'] = person.movie_persons_id.id
					person_json['name'] = person.movie_persons_id.person_name
					movie_pers.append(person_json)
				movie_json['musicdirectors'] = movie_pers

				prof_id=professions.objects.get(profession_name='Producer')
				this_movie_persons=(movie_casting.objects.filter(movie_id=movie.id,profession_id=prof_id.id))
				 		
				movie_pers = []
				for person in this_movie_persons:
					person_json = {}
					person_json['id'] = person.movie_persons_id.id
					person_json['name'] = person.movie_persons_id.person_name
					movie_pers.append(person_json)
				movie_json['producers'] = movie_pers

				movie_per=list(set(movie_per))

			except Exception, e:
				print e
				print movie.id
				movie_json['persons']=''
			
			try:
				this_movie_banner_images=banner_images.objects.filter(movie_id=movie.id)
				this_movie_banner_image=random.choice(this_movie_banner_images)
				this_movie_banner_image_small = get_thumbnail(this_movie_banner_image.imagefile, MOVIE_BANNER_THUMBS_SIZE, crop='center').url
				this_movie_banner_image = this_movie_banner_image.imagefile.url
			except banner_images.DoesNotExist:
				this_movie_banner_image='/media/template_images/no_image.jpg'
				this_movie_banner_image_small = get_thumbnail('template_images/no_image.jpg', MOVIE_BANNER_THUMBS_SIZE, crop='top').url
			except:
				this_movie_banner_image='/media/template_images/no_image.jpg'
				this_movie_banner_image_small = get_thumbnail('template_images/no_image.jpg', MOVIE_BANNER_THUMBS_SIZE, crop='top').url
			
			movie_json['banner_image']=this_movie_banner_image
			movie_json['banner_image_small']=this_movie_banner_image_small
			
			result['movies'].append(movie_json)
		
		result['movies']=sortwithdays(result['movies'])
		result['movies'] = result['movies'][::-1][::-1]
		result_json = json.dumps(result)	

		return HttpResponse(result_json,mimetype)
#	else:
#		return HttpResponse('fail')


def sortpowh(a):
	x=len(a)
	i=0
	#print x
	while(x>1):
		#print x
		i=0
		while(i<(x-1)):
			if a[i]['powh']>a[i+1]['powh']:
				b=a[i]
				a[i]=a[i+1]
				a[i+1]=b
			i=i+1
		x=x-1
	a.reverse()
	return a


def sortwithdays(a):
	x=len(a)
	i=0
	while(x>1):
		i=0
		while(i<(x-1)):
			if a[i]['days_left']>a[i+1]['days_left']:
				b=a[i]
				a[i]=a[i+1]
				a[i+1]=b
			i=i+1
		x=x-1
	return a