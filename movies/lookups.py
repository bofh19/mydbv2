
from django.db.models import Q
from django.utils.html import escape

from ajax_select import LookupChannel

from movies.models import movies


class moviesLookup(LookupChannel):

    model = movies

    def get_query(self,q,request):
        return movies.objects.filter(Q(movie_name__icontains=q)).order_by('movie_name')

    def get_result(self,obj):
        u""" result is the simple text that is the completion of what the person typed """
        return obj.movie_name

    def format_match(self,obj):
        """ (HTML) formatted item for display in the dropdown """
        return self.format_item_display(obj)

    def format_item_display(self,obj):
        """ (HTML) formatted item for displaying item in the selected deck area """
        return u"%s<hr/>" % (escape(obj.movie_name))
