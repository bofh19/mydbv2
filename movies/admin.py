from django.contrib import admin
from django import forms

from movies.models import movies, movie_dates, telugu_movie
from movies.models import movie_extra_info
from movies.models import movie_theater_status
from movie_casting.models import movie_casting
from movie_characters.models import movie_characters
from images.models import background_images
from images.models import banner_images
from movie_music.models import movie_music
from movie_genres.models import movie_genres
from movie_trailers.models import movie_trailers
from wiki_parser.models import movie_wlinks

from ajax_select import make_ajax_form
from ajax_select.admin import AjaxSelectAdmin
from index.global_var import *

from sorl.thumbnail import default
ADMIN_THUMBS_SIZE = '40x40'
ADMIN_THUMBS_SIZE_LARGE = '100x100'

class movie_extra_infoinlie(admin.TabularInline):
	model=movie_extra_info
	extra=1

class tel_movie_infoinlie(admin.TabularInline):
	model=telugu_movie
	extra=1

class movie_trailers_inline(admin.TabularInline):
	model=movie_trailers
	extra=1

class movie_genres_inline(admin.TabularInline):
	model=movie_genres
	form = make_ajax_form(movie_genres,{'genre_id':'genres_lookups'})
	extra=1

class movie_wlinks_inline(admin.TabularInline):
	model=movie_wlinks

class movie_movie_dates_inline(admin.TabularInline):
	model = movie_dates
	extra = 1
	
class movie_theater_status_inline(admin.StackedInline):
    model = movie_theater_status

class movie_characters_inline(admin.TabularInline):
	model=movie_characters
	form = make_ajax_form(movie_characters,{'movie_persons_id':'movie_person_lookups'})
	extra=1

class movie_casting_inline(admin.TabularInline):
	model=movie_casting
	form = make_ajax_form(movie_casting,{'movie_persons_id':'movie_person_lookups','profession_id':'professions_lookups'})
	fields=('movie_persons_id','profession_id','edit_link_person',)
	extra=1
	readonly_fields=('edit_link_person',)

	def edit_link_person(self,obj):
		if obj.id:
			return u'<a href="'+admin_url+'movie_persons/movie_persons/'+str(obj.movie_persons_id.id)+'/\" target=\"_blank\">Edit -'+str(obj.movie_persons_id.person_name)+'- Here</a>'
		else:
			return u'<a href="'+admin_url+'movie_persons/movie_persons/add/\" target=\"_blank\">Add New Person Here</a>'
	edit_link_person.short_description = 'Edit Here'
	edit_link_person.allow_tags = True

class movie_music_inline(admin.TabularInline):
	model=movie_music
	extra=2
	fields = ('song_name','song_track','song_desc','edit_link',)
	readonly_fields=('edit_link',)

	def edit_link(self,obj):
		if obj.id:
			return u'<a href="'+admin_url+'movie_music/movie_music/'+str(obj.id)+'/\" target=\"_blank\">Edit Here</a>'
		else:
			pass
	edit_link.short_description = 'Edit Here'
	edit_link.allow_tags = True

class banner_images_inline(admin.TabularInline):
	model=banner_images
	extra=1
	fields = ('imagefile','user_id', 'user_level','image_thumb',)
	readonly_fields=('image_thumb',)
	def image_thumb(self, obj):
		if obj.imagefile:
			try:
				thumb = default.backend.get_thumbnail(obj.imagefile.file, ADMIN_THUMBS_SIZE)
				return u'<img width="%s" src="%s" />' % (thumb.width, thumb.url)
			except Exception, e:
				pass
		else:
			pass
	image_thumb.short_description = 'My Thumbnail'
	image_thumb.allow_tags = True


class background_images_inline(admin.TabularInline):
	model=background_images
	extra=1
	fields = ('imagefile','user_id', 'user_level','image_thumb',)
	readonly_fields=('image_thumb',)
	def image_thumb(self, obj):
		if obj.imagefile:
			try:
				thumb = default.backend.get_thumbnail(obj.imagefile.file, ADMIN_THUMBS_SIZE_LARGE)
				return u'<img width="%s" src="%s" />' % (thumb.width, thumb.url)
			except Exception, e:
				pass
		else:
			pass
	image_thumb.short_description = 'My Thumbnail'
	image_thumb.allow_tags = True

class movieForm(forms.ModelForm):
	movie_plot=forms.CharField(widget=forms.Textarea)
	movie_summary = forms.CharField(widget=forms.Textarea)
	class Meta:
		model = movies

class movieAdmin(admin.ModelAdmin):
	form = movieForm
	inlines = [
					tel_movie_infoinlie,
					movie_movie_dates_inline,
					movie_wlinks_inline,
					movie_genres_inline,
					movie_theater_status_inline,
					movie_extra_infoinlie,
					movie_trailers_inline,
					movie_casting_inline,
					movie_characters_inline,
					movie_music_inline,
					banner_images_inline,
					background_images_inline
				]
	search_fields = ['movie_name']

admin.site.register(movies, movieAdmin)



#admin.site.register(movies)
admin.site.register(movie_dates)
admin.site.register(movie_extra_info)
#admin.site.register(movie_theater_status)

