import json
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from movies.models import movies
from django.http import HttpResponse
from django.utils.datastructures import MultiValueDictKeyError
from sorl.thumbnail import get_thumbnail
from images.models import banner_images
import random
def movie_search(request):
	return render_to_response('movies/templates/movie_search.html',context_instance=RequestContext(request))

def movie_searching(request):
	if request.is_ajax():
		q=request.GET.get('term','')
		moviez=movies.objects.filter(movie_name__icontains=q)[:10]
		results=[]
		for movie in moviez:
			movie_json={}
			movie_json['id']=movie.id
			movie_json['label']=movie.movie_name
			movie_json['value']=movie.movie_name
			movie_json['movie_id']=movie.id
			movie_json['url']="/movie/"+str(movie.id)+"-"+movie.movie_name+"/index/"
			try:
				this_movie_banner_images=banner_images.objects.filter(movie_id=movie.id)
				this_movie_banner_img=random.choice(this_movie_banner_images)
				this_movie_banner_image=this_movie_banner_img.imagefile
				movie_json['icon'] = get_thumbnail(this_movie_banner_image, '40x40', quality=99).url
				#print movie_json['icon']
			except banner_images.DoesNotExist:
				movie_json['icon']=''
			except Exception, e:
				#print e
				movie_json['icon']=''

			
			results.append(movie_json)
		data=json.dumps(results)
	else:
		data='fail'
	mimetype='application/json'
	return HttpResponse(data, mimetype)
