from django import forms
from django.contrib.admin import widgets 
import datetime

class enter_movie_form(forms.Form):
	movie_name=forms.CharField(max_length=40)
	movie_year=forms.DateField(initial=datetime.datetime.now(),widget=widgets.AdminDateWidget)
	movie_plot=forms.CharField(max_length=200,initial='Not Known')#,widget=forms.Textarea(attrs={'cols': 5, 'rows': 5}))
	movie_summary=forms.CharField(max_length=5000,initial='Not Known')
	movie_banner_image=forms.ImageField(required=False)
	movie_background_image=forms.ImageField(required=False)

