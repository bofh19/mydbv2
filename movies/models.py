from django.db import models
# Create your models here.


class movies(models.Model):
	movie_name=models.CharField(max_length=40,unique=True)
	movie_year=models.DateField(blank=True,null=True,default='1800-01-01')
	movie_plot=models.CharField(max_length=500,blank=True,null=True)
	movie_summary=models.CharField(max_length=5000,blank=True,null=True)
	movie_info=models.TextField(blank=True)
	def __unicode__(self):
		return self.movie_name
	
	def get_absolute_url(self):
		# url="/movie/"+str(self.id)+"-"+str(self.movie_name)+"/index/"  #old
		url="/v1/#/movie/"+str(self.id)+"/"+str(self.movie_name)+"/"
		return url


class telugu_movie(models.Model):
	movie_id = models.ForeignKey(movies)
	tel_movie_name=models.CharField(max_length=40,unique=True,null=True,blank=True)

	def __unicode__(self):
		return self.tel_movie_name +"--" +str(self.movie_id.movie_name)

class movie_dates(models.Model):
	movie_date=models.DateField(default=None)
	movie_id=models.ForeignKey(movies)
	desc = models.CharField(max_length=2000,blank=True,null=True)

class movie_extra_info(models.Model):
	fieldone=models.CharField(max_length=500)
	fieldtwo=models.CharField(max_length=500)
	movie_id=models.ForeignKey(movies)

	def __unicode__(self):
		return self.movie_id.movie_name+"--"+self.fieldone+"=="+self.fieldtwo

class movie_theater_status(models.Model):
	movie_id=models.ForeignKey(movies, unique=True)
	RELEASE_CHOICES = (
		(u'T',u'In Theaters'),
		(u'F',u'Not In Theaters'),
		(u'D',u'Going To Release')
		)
	release_status=models.CharField(max_length=2,choices=RELEASE_CHOICES)

	def __unicode__(self):
		return self.movie_id.movie_name+"---"+self.release_status