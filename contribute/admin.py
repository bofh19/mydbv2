
from django.contrib import admin
from django import forms
from contribute.models import *

from sorl.thumbnail import default
ADMIN_THUMBS_SIZE = '160x120'

class data_imageAdmin(admin.ModelAdmin):
    model = data_image
    search_fields=['movie_id__movie_name','person_id__person_name']
    list_display = ['image_thumb', 'movie_id','person_id' ]

    def image_thumb(self, obj):
        if obj.imagefile:
            thumb = default.backend.get_thumbnail(obj.imagefile.file, ADMIN_THUMBS_SIZE)
            return u'<img width="%s" src="%s" />' % (thumb.width, thumb.url)
        else:
            return "No Image" 
    image_thumb.short_description = 'My Thumbnail'
    image_thumb.allow_tags = True

class data_textForm(forms.ModelForm):
    text = forms.CharField(widget=forms.Textarea)
    class Meta:
        model = data_text


class data_textAdmin(admin.ModelAdmin):
    form=data_textForm
    search_fields=['movie_id__movie_name','person_id__person_name','user_id__username']

admin.site.register(data_image,data_imageAdmin)
admin.site.register(data_text,data_textAdmin)