from django.shortcuts import render_to_response
from django.template import RequestContext

def user_profile_main(request):
	if request.user.is_authenticated():
		ctx = {	
        'last_login': request.session.get('social_auth_last_login_backend')
   				 }
		return render_to_response('user_profile/templates/user_profile_main.html',ctx,context_instance=RequestContext(request))
	else:
		return render_to_response('user_profile/templates/user_profile_main.html',
								{
								},context_instance=RequestContext(request)
							)
"""
from user_profile.models import now_redirect_url
def redirect_page(request):
	#print request.session.session_key
	try:
		sessionid=request.session.session_key
		now_redirect_url=redirect_url.objects.get(session_id=sessionid).redirect_url
	except Exception, e:
		now_redirect_url=''
	return render_to_response('user_profile/templates/redirect_page.html',
			{
			'redirect_url':now_redirect_url,
			},context_instance=RequestContext(request))
"""

import json
from django.http import HttpResponse

def user_profile_api(request):
	result = {}
	mimetype='application/json'
	if request.user.is_authenticated():
		result['username']=request.user.username
		result['email']=request.user.email
	
	result_json = json.dumps(result)
	return HttpResponse(result_json,mimetype)

