from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class eligibility(models.Model):
	user_id=models.ForeignKey(User)
	
	eligible=models.IntegerField(choices=((1,'1'),
										(0,'0'))
							   )
	class Meta:
		unique_together=('user_id','eligible')

	def __unicode__(self):
		username=self.user_id.username
		output=username+"------"+str(self.eligible)
		return output


