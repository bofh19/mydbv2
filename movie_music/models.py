from django.db import models

# Create your models here.
from movies.models import movies
from movie_persons.models import movie_persons


class movie_music(models.Model):
	song_name=models.CharField(max_length=40)
	song_track=models.IntegerField()
	movie_id=models.ForeignKey(movies)
	song_desc=models.CharField(max_length=500,blank=True,null=True)
	def __unicode__(self):
		return self.song_name+"-"+str(self.song_track)+"-"+self.movie_id.movie_name

class song_persons_info(models.Model):
	person_id=models.ForeignKey(movie_persons)
	song_id=models.ForeignKey(movie_music)
	person_desc=models.CharField(max_length=500)
	
	def __unicode__(self):
		return self.song_id.song_name+"--"+self.person_id.person_name

class song_video_info(models.Model):
	song_id=models.ForeignKey(movie_music)
	person_id=models.ForeignKey(movie_persons)
	person_desc=models.CharField(max_length=500)
	class Meta:
		unique_together=('person_id','song_id')

	def __unicode__(self):
		return self.song_id.song_name+"--"+self.person_id.person_name

class song_links(models.Model):
	SONG_CHOICES = (
    ('YouTube', 'YouTube'),
    ('Other','Other')
	)
	TYPE_CHOICES=(
		('Video','Video'),
		('Audio','Audio')
		)
	song_id=models.ForeignKey(movie_music)
	link_type=models.CharField(max_length=15,choices=TYPE_CHOICES)
	link_loc_type=models.CharField(max_length=15,choices=SONG_CHOICES)
	song_url=models.CharField(max_length=2000)

	def __unicode__(self):
		return self.song_id.song_name+"--"+self.song_url


######## songs files loc and cd images ########

class songs_loc(models.Model):
	song_id=models.ForeignKey(movie_music)
	song_loc = models.CharField(max_length=225,unique=True)
	bitrate_choices=(
		('128','128'),
		('256','256'),
		('320','320'),
		)
	bitrate=models.CharField(max_length=3,choices=bitrate_choices)

	class Meta:
		unique_together=('song_id','bitrate')

	def __unicode__(self):
		return self.song_id.movie_id.movie_name+" -- "+self.song_id.song_name

class cd_images(models.Model):
	movie_id=models.ForeignKey(movies)
	cd_image_loc=models.CharField(max_length=225,unique=True)