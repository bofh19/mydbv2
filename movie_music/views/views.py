# Create your views here.\
from django.http import HttpResponse
from movie_music.models import song_links
from movie_music.models import movie_music
from urlparse import urlparse
from django.shortcuts import render_to_response
from django.template import RequestContext
from images.models import background_images
import random
def music_index(request):
	try:
		back_image= random.choice(background_images.objects.all()[::-1][:5])
	except Exception, e:
		back_image=''
	return render_to_response('movie_music/templates/music_index.html',
								{
								'back_image':back_image,
								},context_instance=RequestContext(request)
						)
#
#
import os
from settings import MEDIA_ROOT
def music_list_local_dir(request):
	path=MEDIA_ROOT+'/music'

	structure=[]
	for name in sorted(os.listdir(path)):
		this_dir={}
		if os.path.isdir(os.path.join(path,name)):
			this_dir['dirname']=name
			this_dir_files={}
			this_dir_file_mp3=[]
			this_dir_file_jpg=[]
			for each_file in sorted(os.listdir(os.path.join(path,name))):
				if os.path.isfile(os.path.join(path,name,each_file)):					
					filename,fileext=os.path.splitext(os.path.join(path,name,each_file))
					if fileext=='.mp3' or fileext=='.MP3' or fileext=='.wav' or fileext=='.WAV' or fileext=='.ogg':
						this_dir_file_mp3.append(each_file)
					elif fileext=='.jpg' or fileext=='.png' or fileext=='.jpeg'or fileext=='.JPG'or fileext=='.PNG'or fileext=='.GIF'or fileext=='.gif':
						this_dir_file_jpg.append(each_file)
					
			this_dir_files['mp3']=this_dir_file_mp3
			this_dir_files['jpg']=this_dir_file_jpg

			this_dir['files']=this_dir_files
		structure.append(this_dir)
	return render_to_response('movie_music/templates/music_list_local_dir.html',
								{
								'files':structure,
								},context_instance=RequestContext(request)
						)




import json
mimetype='application/json'
import eyed3
def music_list_local_dir_json(request):
	path=MEDIA_ROOT+'/music'
	structure=[]
	for name in sorted(os.listdir(path)):
		this_dir={}
		if os.path.isdir(os.path.join(path,name)):
			this_dir['dirname']=name
			this_dir_files={}
			this_dir_file_mp3=[]
			this_dir_file_jpg=[]
			for each_file in sorted(os.listdir(os.path.join(path,name))):
				if os.path.isfile(os.path.join(path,name,each_file)):					
					filename,fileext=os.path.splitext(os.path.join(path,name,each_file))
					if fileext=='.mp3' or fileext=='.MP3' or fileext=='.wav' or fileext=='.WAV' or fileext=='.ogg':
						mp3file = {}
						mp3file['name'] = each_file
						mp3file['album'] = None
						mp3file['artist'] = None
						mp3file['genre'] = None
						mp3file['publisher'] = None
						mp3file['title'] = None
						mp3file['track_num'] = None
						mp3file['bit_rate'] = None
						mp3file['size_bytes'] = None
						mp3file['time_secs'] = None
						try:
							f = eyed3.load(os.path.join(path,name,each_file))
							mp3file['album'] = f.tag.album
							mp3file['artist'] = f.tag.artist
							mp3file['genre'] = f.tag.genre.name
							mp3file['publisher'] = f.tag.publisher
							mp3file['title'] = f.tag.title
							mp3file['track_num'] = f.tag.track_num[0]
							mp3file['bit_rate'] = f.info.bit_rate_str
							mp3file['size_bytes'] = f.info.size_bytes
							mp3file['time_secs'] = f.info.time_secs
						except Exception, e:
							print e,"failed on",os.path.join(path,name,each_file)
						
						this_dir_file_mp3.append(mp3file)
					elif fileext=='.jpg' or fileext=='.png' or fileext=='.jpeg'or fileext=='.JPG'or fileext=='.PNG'or fileext=='.GIF'or fileext=='.gif':
						this_dir_file_jpg.append(each_file)
					
			this_dir_files['mp3']=this_dir_file_mp3
			this_dir_files['jpg']=this_dir_file_jpg

			this_dir['files']=this_dir_files
		structure.append(this_dir)
	result_json=json.dumps(structure)
	return HttpResponse(result_json,mimetype)
