from django.http import HttpResponse
from urlparse import urlparse
from django.shortcuts import render_to_response
from django.template import RequestContext
from images.models import background_images, banner_images
from movie_music.models import *
from movies.models import movies

from settings import MEDIA_URL

import random
import json

from django.views.decorators.csrf import csrf_protect
from django.views.decorators.csrf import requires_csrf_token
from django.views.decorators.csrf import ensure_csrf_cookie

from sorl.thumbnail import get_thumbnail
MOVIE_BANNER_SIZE = '200x300'

@ensure_csrf_cookie
@requires_csrf_token
@csrf_protect
def music_player(request):
	
	return render_to_response('movie_music/templates/music_player.html',
								{

								},context_instance=RequestContext(request)
						)

@ensure_csrf_cookie
@requires_csrf_token
@csrf_protect	
def get_songs_movie(request,movie_id):
	result = {}
	result['songs']=[]
	mimetype='application/json'
	result_json=''
	try:
		this_movie=movies.objects.get(id=movie_id)
	except Exception, e:
		result_json=json.dumps(result)
		return HttpResponse(result_json,mimetype)
	
	songs_list=movie_music.objects.filter(movie_id=movie_id).order_by('song_track')
	result['movie_id']=movie_id
	track=1
	for each_song in songs_list:
		this_song={}
		this_song['track']=track
		this_song['id']=each_song.id
		this_song['song_name']=each_song.song_name
		this_song['song_desc']=each_song.song_desc
		track=track+1
		result['songs'].append(this_song)
	try:	
		try:
			this_movie_banner_images=banner_images.objects.filter(movie_id=movie_id)
			this_movie_banner_image=random.choice(this_movie_banner_images)
			result['banner_image']=this_movie_banner_image.imagefile.url
			im = get_thumbnail(this_movie_banner_image.imagefile, MOVIE_BANNER_SIZE, crop='top')
			result['banner_image_small']=im.url
		except banner_images.DoesNotExist:
			result['banner_image']='/meda/template_images/no_image.jpg'
			result['banner_image_small']=get_thumbnail('template_images/no_image.jpg', MOVIE_BANNER__SIZE, crop='top').url
		except Exception, e:
			result['banner_image']='/meda/template_images/no_image.jpg'
			result['banner_image_small']=get_thumbnail('template_images/no_image.jpg', MOVIE_BANNER_SIZE, crop='top').url
	except Exception,e:
		pass
	result_json=json.dumps(result)
	return HttpResponse(result_json,mimetype)


@ensure_csrf_cookie
@requires_csrf_token
@csrf_protect
def get_song_loc(request,song_id):
	result={}
	result['song_loc']=''
	mimetype='application/json'
	this_song_bitrate=''
	try:
		this_song_bitrate=request.GET['bitrate']
	except Exception, e:
		this_song_bitrate=''
	
	try:
		if this_song_bitrate == '':
			this_song=songs_loc.objects.filter(song_id=song_id)[0]
		else:
			this_song=songs_loc.objects.get(song_id=song_id,bitrate=this_song_bitrate)

		result['song_loc']=MEDIA_URL+this_song.song_loc
	except Exception, e:
		result['song_loc']=''
	result['song_id']=song_id
	result_json=json.dumps(result)
	return HttpResponse(result_json,mimetype)