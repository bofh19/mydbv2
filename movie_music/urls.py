from django.conf.urls import patterns, include, url

urlpatterns = patterns('movie_music.views',
    url(r'^$','music_index',name='music_index'), 
    url(r'^ld/$','music_list_local_dir',name='music_list_local_dir'), 
    url(r'^player/$','music_player',name='music_player'), 

    url(r'^json/$','music_list_local_dir_json',name='music_list_local_dir_json'),
    url(r'^player/get_songs_movie/(?P<movie_id>\d+)/$','get_songs_movie',name='get_songs_movie'),
    url(r'^player/get_song_loc/(?P<song_id>\d+)/$','get_song_loc',name='get_song_loc'),
)
