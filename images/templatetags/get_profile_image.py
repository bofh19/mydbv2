from django import template

from images.models import profile_images
import random
from sorl.thumbnail import get_thumbnail

register = template.Library()

class pimage_40x40_Node(template.Node):
	def __init__(self,parser):
		self.parser=parser
	
	def render(self,context):
		try:
			out = random.choice(profile_images.objects.filter(person_id=self.parser))
			out_url = get_thumbnail(out.imagefile, '40x40', quality=99).url
			return out_url
		except Exception, e:
			return ''


@register.simple_tag
def get_profile_image40x40(parser):
	return pimage_40x40_Node(parser)

