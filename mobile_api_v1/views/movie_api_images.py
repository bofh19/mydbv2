# Create your views here.
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect,HttpResponse
from images.models import images_tags
from images.models import images
from images.models import image_seq
from images.forms import *
from django.core.urlresolvers import reverse
from movies.models import movies
from movie_persons.models import movie_persons
from images.models import background_images

mimetype='application/json'

import random
import json
import re

from datetime import *
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.csrf import requires_csrf_token
from django.views.decorators.csrf import ensure_csrf_cookie

# movie_id=7
# url= "http://127.0.0.1:8000/api/mif/"+movie_id+"/"
# $.getJSON(url,function(data){console.log(data)})


from sorl.thumbnail import get_thumbnail
MOVIE_PERSON_THUMBS_SIZE = '80x80'
# MOVIE_BANNER_THUMBS_SIZE = '100x120'
MOVIE_BANNER_THUMBS_SIZE = '100x160'
MOVIE_BANNER_SIZE = '200x300'




@ensure_csrf_cookie
@requires_csrf_token
@csrf_protect
def movie_images_all(request,movie_id):
	result = {}		
	result['images']=[]
	try:
		this_movie=movies.objects.get(id=movie_id)
		name=this_movie.movie_name
	
	except Exception, e:
		result_json = json.dumps(result)	
		return HttpResponse(result_json,mimetype)

	try:
		this_movie_imagez=image_seq.objects.filter(movie_id=movie_id)
		total_images=len(this_movie_imagez)
		if len(this_movie_imagez)>1	:
			this_movie_imagez=this_movie_imagez[::-1]
		this_movie_images=[]
		for a in this_movie_imagez:
			this_movie_images.append(images.objects.get(id=a.image_id.id))
	except Exception, e:
		this_movie_images=[]

	try:
		this_movie_back_imgages=background_images.objects.filter(movie_id=movie_id)
	except Exception, e:
		this_movie_back_imgages=''

	try:
		this_movie_banner_images = banner_images.objects.filter(movie_id=movie_id)
	except Exception, e:
		this_movie_banner_images = ''

	images_all = []
	for each_image in this_movie_images:
		current_image = {}
		current_image['actual'] = each_image.imagefile.url
		try:
			current_image['small'] = get_thumbnail(each_image.imagefile, MOVIE_BANNER_THUMBS_SIZE, crop='center').url
		except:
			current_image['small'] = get_thumbnail('template_images/no_image.jpg', MOVIE_BANNER_THUMBS_SIZE, crop='top').url
		images_all.append(current_image)
	
	for each_image in this_movie_back_imgages:
		current_image = {}
		current_image['actual'] = each_image.imagefile.url
		try:
			current_image['small'] = get_thumbnail(each_image.imagefile, MOVIE_BANNER_THUMBS_SIZE, crop='center').url
		except:
			current_image['small'] = get_thumbnail('template_images/no_image.jpg', MOVIE_BANNER_THUMBS_SIZE, crop='top').url
		images_all.append(current_image)

	for each_image in this_movie_banner_images:
		current_image = {}
		current_image['actual'] = each_image.imagefile.url
		try:
			current_image['small'] = get_thumbnail(each_image.imagefile, MOVIE_BANNER_THUMBS_SIZE, crop='center').url
		except Exception, e:
			current_image['small'] = get_thumbnail('template_images/no_image.jpg', MOVIE_BANNER_THUMBS_SIZE, crop='top').url
		images_all.append(current_image)
		
	result['images']=images_all
	result_json = json.dumps(result)	
	return HttpResponse(result_json,mimetype)