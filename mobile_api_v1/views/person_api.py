from django.http import HttpResponse
from urlparse import urlparse
from django.shortcuts import render_to_response
from django.template import RequestContext
from images.models import background_images
from movie_music.models import *
from movies.models import movies

from movie_trailers.models import movie_trailers
from images.models import banner_images
from movie_genres.models import movie_genres
from genres.models import genres
from movie_persons.models import movie_persons
from movie_casting.models import movie_casting
from professions.models import professions
from movies.models import movie_theater_status
from images.models import profile_images

import random
import json
import re

from datetime import *
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.csrf import requires_csrf_token
from django.views.decorators.csrf import ensure_csrf_cookie



from sorl.thumbnail import get_thumbnail

PERSON_BANNER_SIZE = '80x80'
MOVIE_BANNER_THUMBS_SIZE = '80x80'

from movie_persons.models import movie_persons
from movie_casting.models import movie_casting
from images.models import profile_images

# @ensure_csrf_cookie
# @requires_csrf_token
# @csrf_protect
def v2_person_info(request,person_id):
	# if request.is_ajax():
		result = {}
		mimetype='application/json'

		person_json={}

		try:
			person=movie_persons.objects.get(id=person_id)
		except Exception, e:
			result_json='none'
			return HttpResponse(result_json,mimetype)

		person_json['id']=person.id
		person_json['name']=person.person_name
		person_json['gender'] = person.person_gender
		person_json['bio']=person.person_bio

		try:
			this_person_profile_image = random.choice(profile_images.objects.filter(person_id=person_id))
			person_json['profile_image'] = this_person_profile_image.imagefile.url
			if(this_person_profile_image.imagefile.width > 150):
				person_json['profile_image_small']=get_thumbnail(this_person_profile_image.imagefile,PERSON_BANNER_SIZE,crop='center').url
			else:
				person_json['profile_image_small']=get_thumbnail('template_images/no_image.jpg', PERSON_BANNER_SIZE, crop='top').url
		except Exception, e:
			person_json['profile_image'] = '/media/template_images/no_image.jpg'
			person_json['profile_image_small'] = get_thumbnail('template_images/no_image.jpg', PERSON_BANNER_SIZE, crop='top').url
		
		this_person_movies = movie_casting.objects.filter(movie_persons_id=person_id)
		this_person_movies = list(set(this_person_movies))
		movie_json=[]
		for each_movie in this_person_movies:
			this_movie_json={}
			this_movie_json['id']=each_movie.movie_id.id
			this_movie_json['name']=each_movie.movie_id.movie_name
			try:
				this_movie_banner_image = random.choice(banner_images.objects.filter(movie_id=each_movie.movie_id.id))
				this_movie_json['banner_image'] = this_movie_banner_image.imagefile.url
				this_movie_json['banner_image_small'] = get_thumbnail(this_movie_banner_image.imagefile,MOVIE_BANNER_THUMBS_SIZE,crop='center').url
			except Exception, e:
				this_movie_json['banner_image']	= '/media/template_images/no_image.jpg'
				this_movie_json['banner_image_small']	= get_thumbnail('template_images/no_image.jpg', MOVIE_BANNER_THUMBS_SIZE, crop='top').url
			movie_json.append(this_movie_json)
		person_json['movies']=movie_json[::-1]
		result = person_json
		result_json=json.dumps(result)
		return HttpResponse(result_json,mimetype)
	# return HttpResponse('fail')