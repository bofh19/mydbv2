from django.conf.urls import patterns, include, url

urlpatterns = patterns('mobile_api_v1.views',

	 url(r'^api/m/(?P<movie_id>\d+)/$','v2_m_info',name='v2_m_info'),
	 url(r'^api/m/cast/(?P<movie_id>\d+)/$','v2_m_info_cast',name='v2_m_info_cast'),
	 url(r'^api/m/images/(?P<movie_id>\d+)/$','movie_images_all',name='movie_images_all'),
	
	
	url(r'^$','v2',name='v2'),
	url(r'^api/latestdb/$','latestdb',name='latestdb'),
	url(r'^api/releasing/$','releasing',name='releasing'),
	url(r'^api/intheaters/$','intheaters',name='intheaters'),

	

	#url(r'^api/searching/$','searching',name='searching'),
	url(r'^api/search/all/$','search_all',name='search_all'),
	
	url(r'^api/releasing_images/$','images_main_page',name='images_main_page'),



	
	url(r'^api/p/(?P<person_id>\d+)/$','v2_person_info',name='v2_person_info'),
	url(r'^api/p/images/(?P<person_id>\d+)/$','person_images_all'),


)


