create table items(
item_id int not null auto_increment,
item_desc varchar(255),
item_code varchar(40) not null,
primary key (item_id)
);

select * from items;

insert into items (item_desc,item_code) values('B2B COPIER 70 GSM 21 X 29.7 / 2.18','615580700004004R');

insert into items (item_desc,item_code) values('26ee4a87-7f5c-49a6-b194-e99c9e7fdf80','61558073806754R');

insert into items (item_desc,item_code) values('760b7efe-df69-41c9-8a41-b81633ab20a4','61558072837428R');

insert into items (item_desc,item_code) values('bfea270c-dab8-4b07-b8cc-21298de32d81','6155807616681R');

insert into items (item_desc,item_code) values('cae5cfeb-0b15-4ced-9fbc-ec1a46ea6a4f','61558078178571R');

insert into items (item_desc,item_code) values('a94f44f3-4324-4713-a49a-0e0cd2fc0a93','61558078351781R');

insert into items (item_desc,item_code) values('dff4142e-76da-4dbd-b9a8-0a166ac0b3bc','6155807224438R');

insert into items (item_desc,item_code) values('844ecb7b-5cd7-4def-ab9f-e0b60229c4e9','61558073181767R');

insert into items (item_desc,item_code) values('450be97e-55b6-43a6-8409-ce545a32100b','61558076437541R');

insert into items (item_desc,item_code) values('06cf21df-b197-4bdc-a3a7-ed4b9200008c','61558074024726R');

insert into items (item_desc,item_code) values('eaf8bbd6-9faf-46d4-81d8-4975e6261454','61558076044548R');

insert into items (item_desc,item_code) values('acfab111-e4bd-4a8c-8f0e-81d61dbc6073','6155807398464R');

insert into items (item_desc,item_code) values('fd32d6de-d088-4410-8b2d-74d6649be5f3','61558072105693R');

insert into items (item_desc,item_code) values('a4255eff-bfc9-4b93-b397-f7e158089b9b','61558071663148R');

insert into items (item_desc,item_code) values('f5439b05-8adb-4b25-bfd5-ea3c717d7743','61558074447233R');

insert into items (item_desc,item_code) values('9f3fa847-0aff-4263-9526-28b7e3a9596e','61558078266520R');

insert into items (item_desc,item_code) values('347f3a43-b504-4c50-9209-4f6bbe8e91bc','61558072808134R');

insert into items (item_desc,item_code) values('dc7414cd-9d1e-4d8b-8740-df13414e4ede','61558074620639R');

insert into items (item_desc,item_code) values('2afbc4a6-bccb-4252-80f7-7efc277ec3b2','61558073170196R');

insert into items (item_desc,item_code) values('26f2fb00-18e4-405b-9170-4a8ef96e54e2','61558074760846R');

insert into items (item_desc,item_code) values('bad84cb8-db71-483a-ada6-57daefc19246','61558075177046R');

insert into items (item_desc,item_code) values('0af33395-722f-479e-8d27-8351c9ea01cb','61558072040030R');

insert into items (item_desc,item_code) values('cb2bf511-1fb4-4528-bdb7-01a0d2a89857','61558073978403R');

insert into items (item_desc,item_code) values('0fb53fb8-196a-4bee-8492-7de788801561','61558079077528R');

insert into items (item_desc,item_code) values('cf3fee9c-53b3-4acb-8d46-73b37f643592','61558071425956R');

insert into items (item_desc,item_code) values('4d279162-f143-4699-a002-456eb12fb5ee','61558077465801R');
