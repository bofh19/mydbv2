create table order_details(
order_details_id int not null auto_increment,
item_id int,
order_id int,
puom varchar(45),
pqty varchar(45),
suom varchar(45),
sqty varchar(45),
scheduled_date date,
status varchar(45),
foreign key (item_id) references items(item_id),
foreign key (order_id) references orders(order_id),
primary key(order_details_id)
);

select * from order_details;

insert into order_details(item_id,order_id,puom,pqty,suom,sqty,scheduled_date,status)
values(1,1,'TON','100','ABC','100','2012-12-24','Approved')


