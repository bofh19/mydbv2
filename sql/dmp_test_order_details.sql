CREATE DATABASE  IF NOT EXISTS `dmp_test` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `dmp_test`;
-- MySQL dump 10.13  Distrib 5.5.16, for Win32 (x86)
--
-- Host: localhost    Database: dmp_test
-- ------------------------------------------------------
-- Server version	5.5.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `order_details`
--

DROP TABLE IF EXISTS `order_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_details` (
  `order_details_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `puom` varchar(45) DEFAULT NULL,
  `pqty` varchar(45) DEFAULT NULL,
  `suom` varchar(45) DEFAULT NULL,
  `sqty` varchar(45) DEFAULT NULL,
  `scheduled_date` date DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`order_details_id`),
  KEY `item_id` (`item_id`),
  KEY `order_id` (`order_id`),
  CONSTRAINT `order_details_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `items` (`item_id`),
  CONSTRAINT `order_details_ibfk_2` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_details`
--

LOCK TABLES `order_details` WRITE;
/*!40000 ALTER TABLE `order_details` DISABLE KEYS */;
INSERT INTO `order_details` VALUES (1,10,5,'DUU','112','YWW','660','2012-12-05','Approved'),(2,11,1,'MIY','219','VUC','117','2012-12-26','Approved'),(3,7,13,'ZGC','722','WXL','57','2012-12-14','Approved'),(4,27,1,'YXD','367','JNF','924','2012-12-21','Approved'),(5,4,2,'LUL','776','KCC','312','2012-12-01','Approved'),(6,6,12,'GEZ','764','GUN','877','2012-12-14','Approved'),(7,16,3,'VZV','807','VVX','851','2012-12-06','Approved'),(8,28,5,'NVZ','425','PBK','766','2012-12-20','Approved'),(9,25,8,'NIK','390','OKZ','325','2012-12-01','Approved'),(10,14,10,'FOL','85','NDO','300','2012-12-09','Approved'),(11,11,3,'FAM','625','QYG','650','2012-12-01','Approved'),(12,26,9,'OKK','191','CBL','664','2012-12-14','Approved'),(13,19,14,'IAD','436','YMP','454','2012-12-08','Approved'),(14,20,5,'SMI','920','FXW','169','2012-12-26','Approved'),(15,26,10,'DBG','221','BNW','528','2012-12-28','Approved'),(16,14,10,'CTG','593','HAT','107','2012-12-25','Approved'),(17,10,11,'JRP','852','JUD','35','2012-12-29','Approved'),(18,24,6,'PKC','481','CED','771','2012-12-23','Approved'),(19,9,7,'RQP','792','SVB','740','2012-12-08','Approved'),(20,11,4,'ENP','264','XSD','762','2012-12-17','Approved'),(21,9,1,'CBU','107','KGM','261','2012-12-27','Approved'),(22,18,4,'GQT','946','WIQ','198','2012-12-07','Approved'),(23,11,12,'VFM','737','JYU','181','2012-12-23','Approved'),(24,11,4,'YDY','400','HRT','832','2012-12-01','Approved'),(25,28,14,'WVL','701','GKX','931','2012-12-21','Approved'),(26,10,9,'EIF','791','OHC','193','2012-12-27','Approved'),(27,21,9,'ODP','336','VJX','268','2012-12-24','Approved'),(28,19,14,'CRZ','669','EMT','410','2012-12-15','Approved'),(29,3,10,'WDC','629','SZH','360','2012-12-02','Approved'),(30,20,7,'JKT','922','OPQ','833','2012-12-18','Approved'),(31,16,5,'HWU','109','NBQ','282','2012-12-05','Approved'),(32,1,10,'EES','522','TAJ','410','2012-12-28','Approved'),(33,7,2,'YXB','934','BUX','445','2012-12-13','Approved'),(34,5,6,'UEH','647','MEX','728','2012-12-19','Approved'),(35,29,2,'ZYP','989','KBP','625','2012-12-10','Approved'),(36,5,6,'JHR','394','LXB','376','2012-12-28','Approved'),(37,8,9,'QYS','611','DTV','729','2012-12-06','Approved'),(38,15,4,'AVW','496','AVJ','364','2012-12-17','Approved'),(39,4,4,'QLF','840','ZTX','213','2012-12-01','Approved'),(40,20,9,'BLS','149','YVC','391','2012-12-19','Approved'),(41,26,3,'UVU','834','ONI','919','2012-12-28','Approved'),(42,19,13,'RUA','73','FSN','901','2012-12-22','Approved'),(43,14,12,'IKK','944','OOZ','458','2012-12-28','Approved'),(44,19,9,'ARQ','426','CFN','366','2012-12-06','Approved'),(45,24,3,'WQF','128','INR','306','2012-12-05','Approved'),(46,14,9,'WDI','563','CCU','761','2012-12-03','Approved'),(47,16,2,'OMV','864','QRE','921','2012-12-03','Approved'),(48,20,8,'AXH','144','TGZ','639','2012-12-04','Approved'),(49,2,10,'ANG','970','IBV','857','2012-12-15','Approved'),(50,4,7,'ZXH','903','YON','614','2012-12-26','Approved'),(51,27,14,'KQX','315','DGP','388','2012-12-20','Approved'),(52,21,12,'RQH','524','WER','233','2012-12-27','Approved'),(53,7,5,'SJZ','991','VHF','204','2012-12-08','Approved'),(54,8,9,'PJC','984','QFD','747','2012-12-27','Approved'),(55,18,10,'SYH','229','AJJ','108','2012-12-29','Approved'),(56,9,4,'APH','820','MNZ','146','2012-12-01','Approved'),(57,27,8,'XBX','615','UZW','985','2012-12-15','Approved'),(58,10,10,'RGS','231','UKG','882','2012-12-04','Approved'),(59,17,10,'YOJ','772','NTK','173','2012-12-02','Approved'),(60,11,6,'FKC','364','QQE','195','2012-12-16','Approved'),(61,25,11,'HPK','291','IJK','260','2012-12-22','Approved'),(62,29,11,'AZS','763','XNE','955','2012-12-12','Approved'),(63,18,5,'LOE','798','VRW','48','2012-12-01','Approved'),(64,3,10,'RLB','459','JPW','314','2012-12-22','Approved'),(65,19,9,'AAE','695','KHP','902','2012-12-04','Approved'),(66,19,9,'PMC','559','WAN','75','2012-12-05','Approved'),(67,24,8,'GOL','719','CKH','664','2012-12-29','Approved'),(68,10,3,'XLQ','92','STF','386','2012-12-24','Approved'),(69,22,2,'KBN','109','FDH','38','2012-12-13','Approved'),(70,7,2,'RTW','538','SAN','208','2012-12-01','Approved'),(71,9,3,'TXE','469','DFB','176','2012-12-28','Approved'),(72,5,12,'ACT','111','DOC','38','2012-12-23','Approved'),(73,12,14,'NBP','329','HMS','950','2012-12-06','Approved'),(74,8,7,'KOL','878','AKC','701','2012-12-04','Approved'),(75,24,4,'HUV','799','VKA','414','2012-12-04','Approved'),(76,26,1,'EPV','574','XQV','962','2012-12-04','Approved'),(77,11,8,'NGO','911','EMO','491','2012-12-16','Approved'),(78,8,13,'ZXK','150','GEI','38','2012-12-11','Approved'),(79,11,9,'NEA','380','SDN','619','2012-12-26','Approved'),(80,21,3,'IUF','194','ARG','921','2012-12-21','Approved'),(81,5,6,'RGL','171','QDO','750','2012-12-01','Approved'),(82,13,3,'OES','71','FGA','482','2012-12-20','Approved'),(83,29,9,'GFU','871','UTS','537','2012-12-24','Approved'),(84,8,6,'ZJX','978','WVD','460','2012-12-10','Approved'),(85,9,14,'HDZ','93','NFW','317','2012-12-17','Approved'),(86,6,2,'YQJ','842','XRY','258','2012-12-29','Approved'),(87,13,3,'IVH','494','LFO','412','2012-12-16','Approved'),(88,28,2,'BFN','906','YSS','966','2012-12-05','Approved'),(89,28,3,'KPN','707','TOT','57','2012-12-06','Approved'),(90,20,6,'IXF','400','KAO','32','2012-12-08','Approved'),(91,16,12,'YCF','883','TJS','206','2012-12-09','Approved'),(92,4,6,'EJM','123','JGX','134','2012-12-11','Approved'),(93,16,4,'QIT','508','IVY','336','2012-12-11','Approved'),(94,26,8,'DRI','321','BMZ','153','2012-12-12','Approved'),(95,29,1,'FHY','626','SEA','846','2012-12-13','Approved'),(96,15,1,'XUE','259','PHF','111','2012-12-04','Approved'),(97,7,2,'NKI','985','YRA','599','2012-12-03','Approved'),(98,8,12,'XMW','697','OEJ','874','2012-12-24','Approved'),(99,17,2,'AIU','527','FNV','832','2012-12-27','Approved');
/*!40000 ALTER TABLE `order_details` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-01-03 20:10:20
