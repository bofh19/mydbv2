CREATE DATABASE  IF NOT EXISTS `dmp_test` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `dmp_test`;
-- MySQL dump 10.13  Distrib 5.5.16, for Win32 (x86)
--
-- Host: localhost    Database: dmp_test
-- ------------------------------------------------------
-- Server version	5.5.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(255) NOT NULL,
  `customer_addr` varchar(2000) NOT NULL,
  `customer_type` varchar(3) NOT NULL,
  `customer_number` int(11) NOT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` VALUES (1,'S.K.C.C.PAPER & BOARD PVT LTD','.,, 75/79,OLD HANUMAN LANE,, MUMBAI M.H. 400002, INDIA','DUP',157259),(2,'SHREE COMMERCIAL CORPN','.,, 75/79 OLD HANUMAN LANE, MUMBAI M.H. 400002, INDIA','DUP',157257),(3,'SHREE KRISHNA COMMERCIAL CORPN','., 29, STRAND ROAD,, 2ND FLOOR,, CALCUTTA W.B. 700001, INDIA','DUP',157244),(4,'SHERRY CARBONS PVT LTD',', 53, SHAKTI UDYOG NAGAR, VEOOR VILLAGE, PALGHAR (E) M.H. 401404, INDIA','W&P',167445),(5,'C:S.K.C.C.PAPER & BOARD PVT LTD','.,, 75/79,OLD HANUMAN LANE,, MUMBAI M.H. 400002, INDIA','W&P',167259),(6,'VIMAL EXPORT','125,STAFF ROAD, SAVOY ESTATE, AMBALA CANTT HARYANA, INDIA','W&P',168780),(7,'EDUCATION MATERIAL EXPORTS','201,UDYOG BHAVAN,, SONAWALA MARG,, GOREGAON(E), MUMBAI M.H. 400063, INDIA','W&P',161626),(8,'c1d2','customer one for dealer two','DUP',123535),(9,'c2d2','customer two for dealer two','DUP',123535),(10,'c3d2','customer three for dealer two','DUP',123535),(11,'c4d2','customer four for dealer two','W&P',123535),(12,'c5d2','customer five for dealer two','W&P',123535),(13,'c6d2','customer six for dealer two','W&P',123535);
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dealers`
--

DROP TABLE IF EXISTS `dealers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dealers` (
  `dealer_id` int(11) NOT NULL AUTO_INCREMENT,
  `dealer_name` varchar(255) NOT NULL,
  `dealer_login` varchar(255) NOT NULL,
  PRIMARY KEY (`dealer_id`),
  UNIQUE KEY `dealer_name` (`dealer_name`),
  UNIQUE KEY `dealer_login` (`dealer_login`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dealers`
--

LOCK TABLES `dealers` WRITE;
/*!40000 ALTER TABLE `dealers` DISABLE KEYS */;
INSERT INTO `dealers` VALUES (1,'S.K.C.C.PAPER & BOARD PVT LTD','SKCCPBPL'),(2,'dealer two','dealer2');
/*!40000 ALTER TABLE `dealers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dealers_customers`
--

DROP TABLE IF EXISTS `dealers_customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dealers_customers` (
  `dealers_customers_id` int(11) NOT NULL AUTO_INCREMENT,
  `dealer_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  PRIMARY KEY (`dealers_customers_id`),
  KEY `dealer_id` (`dealer_id`),
  KEY `customer_id` (`customer_id`),
  CONSTRAINT `dealers_customers_ibfk_1` FOREIGN KEY (`dealer_id`) REFERENCES `dealers` (`dealer_id`),
  CONSTRAINT `dealers_customers_ibfk_2` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dealers_customers`
--

LOCK TABLES `dealers_customers` WRITE;
/*!40000 ALTER TABLE `dealers_customers` DISABLE KEYS */;
INSERT INTO `dealers_customers` VALUES (1,1,1),(2,1,2),(3,1,3),(4,1,4),(5,1,5),(6,1,6),(7,1,7),(8,2,8),(9,2,9),(10,2,10),(11,2,11),(12,2,12),(13,2,13);
/*!40000 ALTER TABLE `dealers_customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `items` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_desc` varchar(255) DEFAULT NULL,
  `item_code` varchar(40) NOT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items`
--

LOCK TABLES `items` WRITE;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` VALUES (1,'B2B COPIER 70 GSM 21 X 29.7 / 2.18','615580700004004R'),(2,'B2B COPIER 70 GSM 21 X 29.7 / 2.18','615580700004004R'),(3,'26ee4a87-7f5c-49a6-b194-e99c9e7fdf80','61558073806754R'),(4,'760b7efe-df69-41c9-8a41-b81633ab20a4','61558072837428R'),(5,'bfea270c-dab8-4b07-b8cc-21298de32d81','6155807616681R'),(6,'cae5cfeb-0b15-4ced-9fbc-ec1a46ea6a4f','61558078178571R'),(7,'a94f44f3-4324-4713-a49a-0e0cd2fc0a93','61558078351781R'),(8,'dff4142e-76da-4dbd-b9a8-0a166ac0b3bc','6155807224438R'),(9,'844ecb7b-5cd7-4def-ab9f-e0b60229c4e9','61558073181767R'),(10,'450be97e-55b6-43a6-8409-ce545a32100b','61558076437541R'),(11,'06cf21df-b197-4bdc-a3a7-ed4b9200008c','61558074024726R'),(12,'eaf8bbd6-9faf-46d4-81d8-4975e6261454','61558076044548R'),(13,'acfab111-e4bd-4a8c-8f0e-81d61dbc6073','6155807398464R'),(14,'fd32d6de-d088-4410-8b2d-74d6649be5f3','61558072105693R'),(15,'a4255eff-bfc9-4b93-b397-f7e158089b9b','61558071663148R'),(16,'f5439b05-8adb-4b25-bfd5-ea3c717d7743','61558074447233R'),(17,'9f3fa847-0aff-4263-9526-28b7e3a9596e','61558078266520R'),(18,'347f3a43-b504-4c50-9209-4f6bbe8e91bc','61558072808134R'),(19,'dc7414cd-9d1e-4d8b-8740-df13414e4ede','61558074620639R'),(20,'2afbc4a6-bccb-4252-80f7-7efc277ec3b2','61558073170196R'),(21,'26f2fb00-18e4-405b-9170-4a8ef96e54e2','61558074760846R'),(22,'bad84cb8-db71-483a-ada6-57daefc19246','61558075177046R'),(23,'0af33395-722f-479e-8d27-8351c9ea01cb','61558072040030R'),(24,'cb2bf511-1fb4-4528-bdb7-01a0d2a89857','61558073978403R'),(25,'0fb53fb8-196a-4bee-8492-7de788801561','61558079077528R'),(26,'cf3fee9c-53b3-4acb-8d46-73b37f643592','61558071425956R'),(27,'4d279162-f143-4699-a002-456eb12fb5ee','61558077465801R'),(28,'4d279162-f143-4699-a002-456eb12fb5ee','61558077465801R'),(29,'4d279162-f143-4699-a002-456eb12fb5ee','61558077465801R');
/*!40000 ALTER TABLE `items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_details`
--

DROP TABLE IF EXISTS `order_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_details` (
  `order_details_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `puom` varchar(45) DEFAULT NULL,
  `pqty` varchar(45) DEFAULT NULL,
  `suom` varchar(45) DEFAULT NULL,
  `sqty` varchar(45) DEFAULT NULL,
  `scheduled_date` date DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`order_details_id`),
  KEY `item_id` (`item_id`),
  KEY `order_id` (`order_id`),
  CONSTRAINT `order_details_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `items` (`item_id`),
  CONSTRAINT `order_details_ibfk_2` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_details`
--

LOCK TABLES `order_details` WRITE;
/*!40000 ALTER TABLE `order_details` DISABLE KEYS */;
INSERT INTO `order_details` VALUES (1,10,5,'DUU','112','YWW','660','2012-12-05','Approved'),(2,11,1,'MIY','219','VUC','117','2012-12-26','Approved'),(3,7,13,'ZGC','722','WXL','57','2012-12-14','Approved'),(4,27,1,'YXD','367','JNF','924','2012-12-21','Approved'),(5,4,2,'LUL','776','KCC','312','2012-12-01','Approved'),(6,6,12,'GEZ','764','GUN','877','2012-12-14','Approved'),(7,16,3,'VZV','807','VVX','851','2012-12-06','Approved'),(8,28,5,'NVZ','425','PBK','766','2012-12-20','Approved'),(9,25,8,'NIK','390','OKZ','325','2012-12-01','Approved'),(10,14,10,'FOL','85','NDO','300','2012-12-09','Approved'),(11,11,3,'FAM','625','QYG','650','2012-12-01','Approved'),(12,26,9,'OKK','191','CBL','664','2012-12-14','Approved'),(13,19,14,'IAD','436','YMP','454','2012-12-08','Approved'),(14,20,5,'SMI','920','FXW','169','2012-12-26','Approved'),(15,26,10,'DBG','221','BNW','528','2012-12-28','Approved'),(16,14,10,'CTG','593','HAT','107','2012-12-25','Approved'),(17,10,11,'JRP','852','JUD','35','2012-12-29','Approved'),(18,24,6,'PKC','481','CED','771','2012-12-23','Approved'),(19,9,7,'RQP','792','SVB','740','2012-12-08','Approved'),(20,11,4,'ENP','264','XSD','762','2012-12-17','Approved'),(21,9,1,'CBU','107','KGM','261','2012-12-27','Approved'),(22,18,4,'GQT','946','WIQ','198','2012-12-07','Approved'),(23,11,12,'VFM','737','JYU','181','2012-12-23','Approved'),(24,11,4,'YDY','400','HRT','832','2012-12-01','Approved'),(25,28,14,'WVL','701','GKX','931','2012-12-21','Approved'),(26,10,9,'EIF','791','OHC','193','2012-12-27','Approved'),(27,21,9,'ODP','336','VJX','268','2012-12-24','Approved'),(28,19,14,'CRZ','669','EMT','410','2012-12-15','Approved'),(29,3,10,'WDC','629','SZH','360','2012-12-02','Approved'),(30,20,7,'JKT','922','OPQ','833','2012-12-18','Approved'),(31,16,5,'HWU','109','NBQ','282','2012-12-05','Approved'),(32,1,10,'EES','522','TAJ','410','2012-12-28','Approved'),(33,7,2,'YXB','934','BUX','445','2012-12-13','Approved'),(34,5,6,'UEH','647','MEX','728','2012-12-19','Approved'),(35,29,2,'ZYP','989','KBP','625','2012-12-10','Approved'),(36,5,6,'JHR','394','LXB','376','2012-12-28','Approved'),(37,8,9,'QYS','611','DTV','729','2012-12-06','Approved'),(38,15,4,'AVW','496','AVJ','364','2012-12-17','Approved'),(39,4,4,'QLF','840','ZTX','213','2012-12-01','Approved'),(40,20,9,'BLS','149','YVC','391','2012-12-19','Approved'),(41,26,3,'UVU','834','ONI','919','2012-12-28','Approved'),(42,19,13,'RUA','73','FSN','901','2012-12-22','Approved'),(43,14,12,'IKK','944','OOZ','458','2012-12-28','Approved'),(44,19,9,'ARQ','426','CFN','366','2012-12-06','Approved'),(45,24,3,'WQF','128','INR','306','2012-12-05','Approved'),(46,14,9,'WDI','563','CCU','761','2012-12-03','Approved'),(47,16,2,'OMV','864','QRE','921','2012-12-03','Approved'),(48,20,8,'AXH','144','TGZ','639','2012-12-04','Approved'),(49,2,10,'ANG','970','IBV','857','2012-12-15','Approved'),(50,4,7,'ZXH','903','YON','614','2012-12-26','Approved'),(51,27,14,'KQX','315','DGP','388','2012-12-20','Approved'),(52,21,12,'RQH','524','WER','233','2012-12-27','Approved'),(53,7,5,'SJZ','991','VHF','204','2012-12-08','Approved'),(54,8,9,'PJC','984','QFD','747','2012-12-27','Approved'),(55,18,10,'SYH','229','AJJ','108','2012-12-29','Approved'),(56,9,4,'APH','820','MNZ','146','2012-12-01','Approved'),(57,27,8,'XBX','615','UZW','985','2012-12-15','Approved'),(58,10,10,'RGS','231','UKG','882','2012-12-04','Approved'),(59,17,10,'YOJ','772','NTK','173','2012-12-02','Approved'),(60,11,6,'FKC','364','QQE','195','2012-12-16','Approved'),(61,25,11,'HPK','291','IJK','260','2012-12-22','Approved'),(62,29,11,'AZS','763','XNE','955','2012-12-12','Approved'),(63,18,5,'LOE','798','VRW','48','2012-12-01','Approved'),(64,3,10,'RLB','459','JPW','314','2012-12-22','Approved'),(65,19,9,'AAE','695','KHP','902','2012-12-04','Approved'),(66,19,9,'PMC','559','WAN','75','2012-12-05','Approved'),(67,24,8,'GOL','719','CKH','664','2012-12-29','Approved'),(68,10,3,'XLQ','92','STF','386','2012-12-24','Approved'),(69,22,2,'KBN','109','FDH','38','2012-12-13','Approved'),(70,7,2,'RTW','538','SAN','208','2012-12-01','Approved'),(71,9,3,'TXE','469','DFB','176','2012-12-28','Approved'),(72,5,12,'ACT','111','DOC','38','2012-12-23','Approved'),(73,12,14,'NBP','329','HMS','950','2012-12-06','Approved'),(74,8,7,'KOL','878','AKC','701','2012-12-04','Approved'),(75,24,4,'HUV','799','VKA','414','2012-12-04','Approved'),(76,26,1,'EPV','574','XQV','962','2012-12-04','Approved'),(77,11,8,'NGO','911','EMO','491','2012-12-16','Approved'),(78,8,13,'ZXK','150','GEI','38','2012-12-11','Approved'),(79,11,9,'NEA','380','SDN','619','2012-12-26','Approved'),(80,21,3,'IUF','194','ARG','921','2012-12-21','Approved'),(81,5,6,'RGL','171','QDO','750','2012-12-01','Approved'),(82,13,3,'OES','71','FGA','482','2012-12-20','Approved'),(83,29,9,'GFU','871','UTS','537','2012-12-24','Approved'),(84,8,6,'ZJX','978','WVD','460','2012-12-10','Approved'),(85,9,14,'HDZ','93','NFW','317','2012-12-17','Approved'),(86,6,2,'YQJ','842','XRY','258','2012-12-29','Approved'),(87,13,3,'IVH','494','LFO','412','2012-12-16','Approved'),(88,28,2,'BFN','906','YSS','966','2012-12-05','Approved'),(89,28,3,'KPN','707','TOT','57','2012-12-06','Approved'),(90,20,6,'IXF','400','KAO','32','2012-12-08','Approved'),(91,16,12,'YCF','883','TJS','206','2012-12-09','Approved'),(92,4,6,'EJM','123','JGX','134','2012-12-11','Approved'),(93,16,4,'QIT','508','IVY','336','2012-12-11','Approved'),(94,26,8,'DRI','321','BMZ','153','2012-12-12','Approved'),(95,29,1,'FHY','626','SEA','846','2012-12-13','Approved'),(96,15,1,'XUE','259','PHF','111','2012-12-04','Approved'),(97,7,2,'NKI','985','YRA','599','2012-12-03','Approved'),(98,8,12,'XMW','697','OEJ','874','2012-12-24','Approved'),(99,17,2,'AIU','527','FNV','832','2012-12-27','Approved');
/*!40000 ALTER TABLE `order_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_date` date DEFAULT NULL,
  `order_type` varchar(255) DEFAULT NULL,
  `ship_to_cust_id` int(11) DEFAULT NULL,
  `order_status` varchar(255) DEFAULT NULL,
  `order_number` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`order_id`),
  KEY `ship_to_cust_id` (`ship_to_cust_id`),
  CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`ship_to_cust_id`) REFERENCES `customers` (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (1,'2012-12-01','WP-CST-1',1,'BOOKED','201200001'),(2,'2012-12-02','WP-CST-1',2,'Entered','201200002'),(3,'2012-12-03','WP-CST-1',3,'Closed','201200003'),(4,'2012-12-03','WP-CST-1',3,'Closed','201200003'),(5,'2012-12-04','WP-CST-3',4,'Closed','201200004'),(6,'2012-12-05','WP-CST-2',5,'Booked','201200005'),(7,'2012-12-05','WP-CST-2',6,'Booked','201200006'),(8,'2012-12-05','WP-CST-2',7,'Booked','201200007'),(9,'2012-12-08','WP-CST-2',8,'Booked','201200008'),(10,'2012-12-09','WP-CST-2',9,'Booked','201200009'),(11,'2012-12-10','WP-CST-2',10,'Booked','2012000010'),(12,'2012-12-11','WP-CST-2',11,'Booked','2012000011'),(13,'2012-12-12','WP-CST-2',12,'Booked','2012000012'),(14,'2012-12-13','WP-CST-2',13,'Booked','2012000013');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-01-03 20:15:03
