CREATE DATABASE  IF NOT EXISTS `dmp_test` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `dmp_test`;
-- MySQL dump 10.13  Distrib 5.5.16, for Win32 (x86)
--
-- Host: localhost    Database: dmp_test
-- ------------------------------------------------------
-- Server version	5.5.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(255) NOT NULL,
  `customer_addr` varchar(2000) NOT NULL,
  `customer_type` varchar(3) NOT NULL,
  `customer_number` int(11) NOT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` VALUES (1,'S.K.C.C.PAPER & BOARD PVT LTD','.,, 75/79,OLD HANUMAN LANE,, MUMBAI M.H. 400002, INDIA','DUP',157259),(2,'SHREE COMMERCIAL CORPN','.,, 75/79 OLD HANUMAN LANE, MUMBAI M.H. 400002, INDIA','DUP',157257),(3,'SHREE KRISHNA COMMERCIAL CORPN','., 29, STRAND ROAD,, 2ND FLOOR,, CALCUTTA W.B. 700001, INDIA','DUP',157244),(4,'SHERRY CARBONS PVT LTD',', 53, SHAKTI UDYOG NAGAR, VEOOR VILLAGE, PALGHAR (E) M.H. 401404, INDIA','W&P',167445),(5,'C:S.K.C.C.PAPER & BOARD PVT LTD','.,, 75/79,OLD HANUMAN LANE,, MUMBAI M.H. 400002, INDIA','W&P',167259),(6,'VIMAL EXPORT','125,STAFF ROAD, SAVOY ESTATE, AMBALA CANTT HARYANA, INDIA','W&P',168780),(7,'EDUCATION MATERIAL EXPORTS','201,UDYOG BHAVAN,, SONAWALA MARG,, GOREGAON(E), MUMBAI M.H. 400063, INDIA','W&P',161626),(8,'c1d2','customer one for dealer two','DUP',123535),(9,'c2d2','customer two for dealer two','DUP',123535),(10,'c3d2','customer three for dealer two','DUP',123535),(11,'c4d2','customer four for dealer two','W&P',123535),(12,'c5d2','customer five for dealer two','W&P',123535),(13,'c6d2','customer six for dealer two','W&P',123535);
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-01-03 20:10:16
