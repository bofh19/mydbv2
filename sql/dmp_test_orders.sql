CREATE DATABASE  IF NOT EXISTS `dmp_test` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `dmp_test`;
-- MySQL dump 10.13  Distrib 5.5.16, for Win32 (x86)
--
-- Host: localhost    Database: dmp_test
-- ------------------------------------------------------
-- Server version	5.5.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_date` date DEFAULT NULL,
  `order_type` varchar(255) DEFAULT NULL,
  `ship_to_cust_id` int(11) DEFAULT NULL,
  `order_status` varchar(255) DEFAULT NULL,
  `order_number` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`order_id`),
  KEY `ship_to_cust_id` (`ship_to_cust_id`),
  CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`ship_to_cust_id`) REFERENCES `customers` (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (1,'2012-12-01','WP-CST-1',1,'BOOKED','201200001'),(2,'2012-12-02','WP-CST-1',2,'Entered','201200002'),(3,'2012-12-03','WP-CST-1',3,'Closed','201200003'),(4,'2012-12-03','WP-CST-1',3,'Closed','201200003'),(5,'2012-12-04','WP-CST-3',4,'Closed','201200004'),(6,'2012-12-05','WP-CST-2',5,'Booked','201200005'),(7,'2012-12-05','WP-CST-2',6,'Booked','201200006'),(8,'2012-12-05','WP-CST-2',7,'Booked','201200007'),(9,'2012-12-08','WP-CST-2',8,'Booked','201200008'),(10,'2012-12-09','WP-CST-2',9,'Booked','201200009'),(11,'2012-12-10','WP-CST-2',10,'Booked','2012000010'),(12,'2012-12-11','WP-CST-2',11,'Booked','2012000011'),(13,'2012-12-12','WP-CST-2',12,'Booked','2012000012'),(14,'2012-12-13','WP-CST-2',13,'Booked','2012000013');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-01-03 20:10:22
