
from django.contrib import admin
from movie_news.models import movie_news
from django import forms

from ajax_select import make_ajax_form
from ajax_select.admin import AjaxSelectAdmin
from ajax_select import make_ajax_field

class movie_newsForm(forms.ModelForm):
	news_source=forms.CharField(widget=forms.Textarea,required=False)
	news = forms.CharField(widget=forms.Textarea,required=False)

	class Meta:
		model = movie_news

	movie_id  = make_ajax_field(movie_news,'movie_id','movies_lookups',help_text=None)
	
class movie_newsAdmin(admin.ModelAdmin):
	form = movie_newsForm

	search_fields = ['movie_name']

admin.site.register(movie_news, movie_newsAdmin)

