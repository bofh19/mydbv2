from django.db import models

# Create your models here.
class professions(models.Model):
	profession_name=models.CharField(max_length=40)
	profession_catagory=models.CharField(max_length=40,default='Movie')
	profession_level=models.IntegerField()
	profession_desc=models.CharField(max_length=40,unique=True)
	def __unicode__(self):
		return self.profession_name+" "+str(self.profession_level) + "----"+self.profession_desc+" - "+self.profession_catagory
	#/prof/
	def get_absolute_url(self):
		return "/prof/%i-%s/" % self.id,self.profession_name