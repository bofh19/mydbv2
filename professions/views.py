# Create your views here.
import json
from django.shortcuts import get_object_or_404
from django.template import RequestContext
from django.http import HttpResponse
from django.utils.datastructures import MultiValueDictKeyError
from professions.models import professions
from django.shortcuts import render_to_response

def profession_searching(request):
	if request.is_ajax():
		q=request.GET.get('term','')
		professionz=professions.objects.filter(profession_desc__icontains=q)[:20]
		results=[]
		for profession in professionz:
			profession_json={}
			profession_json['id']=profession.id
			profession_json['label']=profession.profession_desc
			profession_json['value']=profession.profession_desc
			results.append(profession_json)
		data=json.dumps(results)
	else:
		data='fail'
	mimetype='application/json'
	return HttpResponse(data, mimetype)
