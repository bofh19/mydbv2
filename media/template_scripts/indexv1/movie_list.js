function loadtabs(){
    var template = $('#list_items').html();
                // console.log(template)
                var data = {};
                var html = Mustache.to_html(template,data);
                $('#main_body').html(html);
}
function loadmovies(){
                loadtabs();
                loadintheaters();
}
function loadintheaters(){
    url = "/v1/api/intheaters/"

    var template = $('#loading_bar').html()
    var data = {}
    var html = Mustache.to_html(template,data);
    $('#p2_main_body').html(html);

    document.getElementById("intheaters_menu").className = "active";
    document.getElementById("upcomming_menu").className = " ";
    document.getElementById("latestdb_menu").className = " ";

    drawlist(url);
}

function loadlatestdb(){
    var template = $('#loading_bar').html()
    var data = {}
    var html = Mustache.to_html(template,data);
    $('#p2_main_body').html(html);

    document.getElementById("intheaters_menu").className = " ";
    document.getElementById("upcomming_menu").className = " ";
    document.getElementById("latestdb_menu").className = "active";

    url = "/v1/api/latestdb/";
    drawlist(url);
}
function loadupcomming(){
    var template = $('#loading_bar').html()
    var data = {}
    var html = Mustache.to_html(template,data);
    $('#p2_main_body').html(html);

    document.getElementById("intheaters_menu").className = " ";
    document.getElementById("upcomming_menu").className = "active";
    document.getElementById("latestdb_menu").className = " ";

    url = "/v1/api/upcomming/";
    drawlist(url);
}


function drawlist(url){
    $.getJSON(url, function(data) {

        var template=$('#movie_items').html();

         var partials = {
                        genre: "<li>[[name]]</li>",
                        hero: "<li><a href=\"#/person/[[id]]/[[name]]/\">[[name]]</a><li>",
                        heroine: "<li><a href=\"#/person/[[id]]/[[name]]/\">[[name]]</a><li>",
                        producer: "<li><a href=\"#/person/[[id]]/[[name]]/\">[[name]]</a><li>",
                        musicdirector: "<li><a href=\"#/person/[[id]]/[[name]]/\">[[name]]</a><li>",
                            };
        var html = Mustache.to_html(template, data, partials);


        $('#p2_main_body').html(html);
        
});
}