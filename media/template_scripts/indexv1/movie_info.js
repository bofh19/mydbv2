
function drawmovie (id) {

    var template = $('#loading_bar_movie').html()
    var data = {}
    var html = Mustache.to_html(template,data);
    $('#main_body').html(html);

	 url = "/v1/api/m/"+id+"/";
    $.getJSON(url, function(data) {

        var template=$('#movie_info').html();
        
        var partials = {
                        profession: "<a href=\"/prof/[[id]]/\">[[name]]</a> ",
                        hero: "<li>[[name]]<li>",
                        each_date: "[[day]] / [[month]] / [[year]] - [[desc]]<br/>",
                         genre: "<li>[[name]]</li>",
                            };
        var html = Mustache.to_html(template, data, partials);
        $('#main_body').html(html);

        $.each(data.trailers, function(i,trailer){
            getYouTubeInfo(trailer.youtube_code)
        });

        drawmovieimages(id);
});
}

function drawmovieimages (id) {

	 var template = $('#loading_bar_movie').html()
    var data = {}
    var html = Mustache.to_html(template,data);
    $('#main_body').append(html);

	 url = "/v1/api/m/images/"+id+"/";
    $.getJSON(url, function(data) {

        var template=$('#movie_images_all').html();
        
        var html = Mustache.to_html(template, data);
        $('#main_body').append(html);
        $('#loading_bar_movie').remove();
});	
}

function getYouTubeInfo(video_id) {
                $.ajax({
                        url: "http://gdata.youtube.com/feeds/api/videos/"+video_id+"?v=2&alt=json",
                        dataType: "jsonp",
                        success: function (data) { 
                          parseresults(data,video_id); 
                        }
                });
        }

function parseresults(data,video_id) {
                var title = data.entry.title.$t;
                var description = data.entry.media$group.media$description.$t;
                var viewcount = data.entry.yt$statistics.viewCount;
                var author = data.entry.author[0].name.$t;
                //var pubdate=data.entry.published.$t;

  var pubdate = new Date( data.entry[ "published" ].$t.substr( 0, 4 ), data.entry[ "published" ].$t.substr( 5, 2 ) - 1, data.entry[ "published" ].$t.substr( 8, 2 ) ).toLocaleDateString( )

              //  document.getElementById('jsmessage').innerHTML+=video_id;
                var title_id='#trailer_image_title_'+video_id;
                // var description_id='#description'+video_id;
                // var extrainfo_id='#extrainfo'+video_id;
                // var pubdate_id='#pubdate'+video_id;

                $(title_id).html(title);
                // $(pubdate_id).html('<b>Added on: </b>'+pubdate)
                // $(description_id).html('<b>Description</b>: ' + description);
                // $(extrainfo_id).html('<b>Author</b>: ' + author + '<br/><b>Views</b>: ' + viewcount);
                // mansory_thumbnails();
             //   getComments(data.entry.gd$comments.gd$feedLink.href + '&max-results=50&alt=json', 1);
}
