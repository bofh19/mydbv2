
$(function() {
$("#movies").autocomplete({
minLength: 2,
      source: "/v1/api/searching/all/",
      focus: function( event, ui ) {
        $( "#movies" ).val( ui.item.label );
        return false;
      },
      select: function( event, ui ) {
                  { 
            window.location.hash = ui.item.hash;
        }   
      }
    })
    .data( "autocomplete" )._renderItem = function( ul, item ) {
       var t = item.label.replace(new RegExp('(' + this.term + ')', 'gi'), "<span style='font-weight:bold;color:Blue;'>$1</span>");
      return $( "<li></li>" )
        .data( "item.autocomplete", item )
        .append( "<a><div class='auto_image'><img src='"+item.icon+"'></div>&nbsp;<div class='auto_text'>"+ t + "</div><br/><div class='auto_text_desc'>"+ item.desc + "</div></a>" )

        .appendTo( ul );
    };
  });

function drawsearchall(q){

  var template = $('#loading_bar_movie').html()
    var data = {}
    var html = Mustache.to_html(template,data);
    $('#main_body').html(html);

  url = '/v1/api/search/all/?q='+q;
  $.getJSON(url, function(data) {

        var template=$('#search_all_results').html();
        var html = Mustache.to_html(template,data);

        console.log(data)
        $('#main_body').html(html);
        
});
  
}  

function drawaboutus () {
    var template = $('#about-us').html();
    var data={};
    var html = Mustache.to_html(template,data);
    $('#main_body').html(html);
}
function drawcontactus () {
    var template = $('#contact-us').html();
    var data={};
    var html = Mustache.to_html(template,data);
    $('#main_body').html(html);
}
