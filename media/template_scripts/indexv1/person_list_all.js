function drawpersonslist(){
    url = '/v1/api/allpersons/'
    var template = $('#loading_bar_movie').html()
    var data = {}
    var html = Mustache.to_html(template,data);
    $('#main_body').html(html);
    $('#loading_bar_movie_text').html("Loading Data....<strong>This will take some time</strong>")
    $.getJSON(url, function(data) {

        var template=$('#persons_list_all').html();

        var html = Mustache.to_html(template, data);

        $('#main_body').html(html);
});
}