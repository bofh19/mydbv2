
function showtrailers(){
    $('#movie_trailers').toggle();
}
 
function ajaxFunctionLoadiFrame(video_id) {
  var getdate = new Date();  
  document.getElementById(video_id).style.display="";
  document.getElementById(video_id).src="http://www.youtube.com/embed/"+video_id;
  image_id="img_"+video_id;
  document.getElementById(image_id).style.display="none";
}

function getYouTubeInfo(video_id) {
                $.ajax({
                        url: "http://gdata.youtube.com/feeds/api/videos/"+video_id+"?v=2&alt=json",
                        dataType: "jsonp",
                        success: function (data) { 
                          parseresults(data,video_id); 
                        }
                });
        }

        function parseresults(data,video_id) {
                var title = data.entry.title.$t;
                var description = data.entry.media$group.media$description.$t;
                var viewcount = data.entry.yt$statistics.viewCount;
                var author = data.entry.author[0].name.$t;
                //var pubdate=data.entry.published.$t;

  var pubdate = new Date( data.entry[ "published" ].$t.substr( 0, 4 ), data.entry[ "published" ].$t.substr( 5, 2 ) - 1, data.entry[ "published" ].$t.substr( 8, 2 ) ).toLocaleDateString( )

              //  document.getElementById('jsmessage').innerHTML+=video_id;
                var title_id='#title'+video_id;
                var description_id='#description'+video_id;
                var extrainfo_id='#extrainfo'+video_id;
                var pubdate_id='#pubdate'+video_id;

                $(title_id).html(title);
                $(pubdate_id).html('<b>Added on: </b>'+pubdate)
                $(description_id).html('<b>Description</b>: ' + description);
                $(extrainfo_id).html('<b>Author</b>: ' + author + '<br/><b>Views</b>: ' + viewcount);
                mansory_thumbnails();
             //   getComments(data.entry.gd$comments.gd$feedLink.href + '&max-results=50&alt=json', 1);
        }

        function mansory_thumbnails(){
                  var $container = $('.thumbnails');
                  $container.imagesLoaded(
                            function(){
                                $container.masonry({
                                    itemSelector : '.span4',
                                  });}
                              );   
                  }


        function getComments(commentsURL, startIndex) {
                $.ajax({
                        url: commentsURL + '&start-index=' + startIndex,
                        dataType: "jsonp",
                        success: function (data) {
                        $.each(data.feed.entry, function(key, val) {
                                $('#comments').append('<br/>Author: ' + val.author[0].name.$t + ', Comment: ' + val.content.$t);
                        });
                        if ($(data.feed.entry).size() == 50) { getComments(commentsURL, startIndex + 50); }
                        }
                });
        }
