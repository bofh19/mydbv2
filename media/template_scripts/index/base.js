
$(function() {
$("#movies").autocomplete({
minLength: 2,
      source: "/v1/api/searching/all/",
      focus: function( event, ui ) {
        $( "#movies" ).val( ui.item.label );
        return false;
      },
      select: function( event, ui ) {
                  { 
            window.location.href = ui.item.href;
        }   
      }
    })
    .data( "autocomplete" )._renderItem = function( ul, item ) {
       var t = item.label.replace(new RegExp('(' + this.term + ')', 'gi'), "<span style='font-weight:bold;color:Blue;'>$1</span>");
      return $( "<li></li>" )
        .data( "item.autocomplete", item )
        .append( "<a><div class='auto_image'><img src='"+item.icon+"'></div>&nbsp;<div class='auto_text'>"+ t + "</div><br/><div class='auto_text_desc'>"+ item.desc + "</div></a>" )

        .appendTo( ul );
    };
  });

function drawaboutus () {
    var template = $('#about-us').html();
    var data={};
    var html = Mustache.to_html(template,data);
    $('#main_body').html(html);
}
function drawcontactus () {
    var template = $('#contact-us').html();
    var data={};
    var html = Mustache.to_html(template,data);
    $('#main_body').html(html);
}
