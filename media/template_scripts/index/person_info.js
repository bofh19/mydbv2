
function drawperson (id) {

    var template = $('#loading_bar_movie').html()
    var data = {}
    var html = Mustache.to_html(template,data);
    $('#main_body').html(html);

	 url = "/v1/api/p/"+id+"/";
    $.getJSON(url, function(data) {

        var template=$('#person_info').html();
        
        var partials = {
                        profession: "<a href=\"/prof/[[id]]/\">[[name]]</a> ",
                            };
        var html = Mustache.to_html(template, data, partials);
        $('#main_body').html(html);
        drawpersonimages(id);
});
}

function drawpersonimages (id) {

	 var template = $('#loading_bar_movie').html()
    var data = {}
    var html = Mustache.to_html(template,data);
    $('#main_body').append(html);

	 url = "/v1/api/p/images/"+id+"/";
    $.getJSON(url, function(data) {

        var template=$('#person_images_all').html();
        
        var html = Mustache.to_html(template, data);
        $('#main_body').append(html);
        $('#loading_bar_movie').remove();
});	
}