from movie_ratings.models import movie_ratings, rating_cat
from django.contrib import admin

admin.site.register(movie_ratings)
admin.site.register(rating_cat)