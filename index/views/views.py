# Create your views here.
from django.shortcuts import render_to_response
from django.template import RequestContext

from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse

from movies.models import movies
from movie_trailers.models import movie_trailers
from images.models import banner_images
import random
from movie_genres.models import movie_genres
from genres.models import genres
from movie_persons.models import movie_persons
from movie_casting.models import movie_casting
from professions.models import professions
from movies.models import movie_theater_status
from images.models import profile_images
from images.models import background_images
from index.models import featured

def main_page(request):
	global_back_image=random.choice(background_images.objects.all())
	if request.user.is_authenticated():
		last_login = request.session.get('social_auth_last_login_backend')
	else:
		last_login=''
	try:
		back_images=background_images.objects.all()#.order_by('-id')[:10]
		back_image=random.choice(back_images)
	except Exception, e:
		back_image=''
	featured_movies=[]
	featured_persons=[]
	try:
		featured_movies=featured.objects.filter(movie_id__isnull=False).order_by('-id')[:4]
	except Exception, e:
		print e
		featured_movies=[]
	try:
		featured_persons=featured.objects.filter(person_id__isnull=False).order_by('-id')[:4]
	except Exception, e:
		featured_persons=[]
	
	return render_to_response('index/templates/main_page.html',
								{
								'last_login':last_login,
								'back_image':back_image,
								'movies':featured_movies,
								'persons':featured_persons,
								},context_instance=RequestContext(request)
							)

