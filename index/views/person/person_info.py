from django.http import HttpResponse
from urlparse import urlparse
from django.shortcuts import render_to_response
from django.template import RequestContext
from images.models import background_images
from movie_music.models import *
from movies.models import movies

from movie_trailers.models import movie_trailers
from images.models import banner_images
from movie_genres.models import movie_genres
from genres.models import genres
from movie_persons.models import movie_persons
from movie_casting.models import movie_casting
from professions.models import professions
from movies.models import movie_theater_status
from images.models import profile_images
from movie_characters.models import movie_characters

import random
import json
import re

from datetime import *
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.csrf import requires_csrf_token
from django.views.decorators.csrf import ensure_csrf_cookie



from sorl.thumbnail import get_thumbnail

PERSON_BANNER_SIZE = '220x320'
MOVIE_BANNER_THUMBS_SIZE = '80x80'

from movie_persons.models import movie_persons
from movie_casting.models import movie_casting
from images.models import profile_images

from datetime import *
# @ensure_csrf_cookie
# @requires_csrf_token
# @csrf_protect
def p_info(request,person_id):
	# if request.is_ajax():
		result = {}
		mimetype='application/json'

		person_json={}

		try:
			person=movie_persons.objects.get(id=person_id)
		except Exception, e:
			result_json='none'
			return HttpResponse(result_json,mimetype)

		person_json['id']=person.id
		person_json['name']=person.person_name
		person_json['gender'] = person.person_gender
		person_json['bio']=person.person_bio
		person_json['info'] = person.person_info
		person_json['sp'] = person.person_sp
		person_json['dob'] = {}
		person_json['dob']['year'] = person.person_date_of_birth.year
		if person_json['dob']['year'] == 1800:
			person_json['dob']['known'] = False
		else:
			person_json['dob']['known'] = True
			person_json['dob']['day'] = person.person_date_of_birth.day
			person_json['dob']['month'] = person.person_date_of_birth.month
			now = date.today()
			person_json['dob']['age'] = now.year - person_json['dob']['year']
		try:
			this_person_profile_image = random.choice(profile_images.objects.filter(person_id=person_id))
			person_json['profile_image'] = this_person_profile_image.imagefile.url
			if(this_person_profile_image.imagefile.width > 150):
				person_json['profile_image_small']=get_thumbnail(this_person_profile_image.imagefile,PERSON_BANNER_SIZE,crop='center').url
			else:
				person_json['profile_image_small']=get_thumbnail('template_images/no_image.jpg', PERSON_BANNER_SIZE, crop='top').url
		except Exception, e:
			person_json['profile_image'] = '/media/template_images/no_image.jpg'
			person_json['profile_image_small'] = get_thumbnail('template_images/no_image.jpg', PERSON_BANNER_SIZE, crop='top').url
		
		this_person_movies = movie_casting.objects.filter(movie_persons_id=person_id).distinct().order_by('movie_id__movie_year')
		# this_person_movies = list(set(this_person_movies))
		movie_json=[]
		for each_movie in this_person_movies:
			this_movie_json={}
			this_movie_json['id']=each_movie.movie_id.id
			this_movie_json['name']=each_movie.movie_id.movie_name
			this_movie_chars_json = {}
			try:
				this_person_char = movie_characters.objects.get(movie_persons_id=person_id,movie_id=each_movie.movie_id.id)
				this_movie_chars_json['name'] = this_person_char.character_name
				this_movie_chars_json['id'] = this_person_char.id
			except Exception, e:
				this_movie_chars_json['name'] = ""
				this_movie_chars_json['id'] = ""
			this_movie_json['character'] = this_movie_chars_json
			if each_movie.movie_id.movie_year.year > 1800:
				this_movie_json['year'] = each_movie.movie_id.movie_year.year
			else:
				this_movie_json['year'] = ''
			
			this_movie_json['professions'] = []
			this_person_movie_profs = movie_casting.objects.filter(movie_persons_id=person_id,movie_id=each_movie.movie_id.id)
			for this_person_movie_profs_each in this_person_movie_profs:
				this_movie_professions_json={}
				this_movie_professions_json['name'] = this_person_movie_profs_each.profession_id.profession_name
				this_movie_professions_json['id'] = this_person_movie_profs_each.profession_id.id
				this_movie_json['professions'].append(this_movie_professions_json)
			try:
				this_movie_banner_image = random.choice(banner_images.objects.filter(movie_id=each_movie.movie_id.id))
				this_movie_json['banner_image'] = this_movie_banner_image.imagefile.url
				this_movie_json['banner_image_small'] = get_thumbnail(this_movie_banner_image.imagefile,MOVIE_BANNER_THUMBS_SIZE,crop='center').url
			except Exception, e:
				this_movie_json['banner_image']	= '/media/template_images/no_image.jpg'
				this_movie_json['banner_image_small']	= get_thumbnail('template_images/no_image.jpg', MOVIE_BANNER_THUMBS_SIZE, crop='top').url
			movie_json.append(this_movie_json)
		person_json['movies']=movie_json[::-1]
		result = person_json
		result['images'] = person_images_all(person_id)
		return render_to_response(
				'index/templates/person/person_info.html',
				{
				 'person_info':result,
				},
				context_instance=RequestContext(request))
	# return HttpResponse('fail')



# Create your views here.
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect,HttpResponse
from images.models import images_tags
from images.models import images
from images.models import image_seq
from images.forms import *
from django.core.urlresolvers import reverse
from movies.models import movies

from movie_persons.models import movie_persons
from images.models import profile_images


mimetype='application/json'

import random
import json
import re

from datetime import *
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.csrf import requires_csrf_token
from django.views.decorators.csrf import ensure_csrf_cookie

from sorl.thumbnail import get_thumbnail



def person_images_all(person_id):
	MOVIE_PERSON_THUMBS_SIZE = '80x80'
	MOVIE_BANNER_THUMBS_SIZE = '100x120'
	MOVIE_BANNER_SIZE = '200x300'
	result={}
	result['images']=[]
	images_all=[]
	try:
		this_person=movie_persons.objects.get(id=person_id)
		name=this_person
	except Exception, e:
		this_person=''
		this_person_images=''

	try:
		this_person_profile_imagez=profile_images.objects.filter(person_id=person_id)
		# this_person_profile_images=[]
		for x in this_person_profile_imagez:
			current_image = {}
			current_image['actual'] = x.imagefile.url
			try:
				current_image['small'] = get_thumbnail(x.imagefile,MOVIE_BANNER_THUMBS_SIZE,crop='center').url
			except Exception, e:
				current_image['small'] = get_thumbnail('template_images/no_image.jpg', MOVIE_BANNER_THUMBS_SIZE, crop='top').url
			images_all.append(current_image)
			# this_person_profile_images.append(x)
	except Exception, e:
		pass
	
	try:
		this_person_imagez=images_tags.objects.filter(person_id=person_id)
		if len(this_person_imagez)>1:
			this_person_imagez=this_person_imagez[::-1]
		# this_person_images=[]

		for a in this_person_imagez:
			current_image = {}
			try:
				img = images.objects.get(id=a.image_id.id)
			except Exception, e:
				img=''
				break
			current_image['actual'] = img.imagefile.url
			try:
				current_image['small'] = get_thumbnail(img.imagefile, MOVIE_BANNER_THUMBS_SIZE, crop='center').url
			except:
				current_image['small'] = get_thumbnail('template_images/no_image.jpg', MOVIE_BANNER_THUMBS_SIZE, crop='top').url
			images_all.append(current_image)
			
			# this_person_images.append(images.objects.get(id=a.image_id.id))
	except Exception, e:
		pass
	
	result['images']=images_all
	return images_all