from django.utils.datastructures import MultiValueDictKeyError
from sorl.thumbnail import get_thumbnail
from images.models import *
from movies.models import movies
from movie_persons.models import movie_persons
from django.http import HttpResponse
import json
import random

from django.shortcuts import render_to_response
from django.template import RequestContext

def search_all(request):
	data={}
	data['results'] = []
	mimetype='application/json'
	try:
		q=request.GET.get('q')
	except Exception, e:
		pass
	
	if q==None:
		return HttpResponse(data, mimetype)

	if len(q)<2:
		return HttpResponse(data, mimetype)

	
	moviez=movies.objects.filter(movie_name__icontains=q)[::-1]
	results=[]
	for movie in moviez:
		movie_json={}
		movie_json['id']=movie.id
		movie_json['label']=movie.movie_name
		if(movie.movie_year.year != 1800):
			movie_json['label'] = movie_json['label']+" ("+str(movie.movie_year.year)+")"
		movie_json['value']=movie.movie_name
		# movie_json['url']="/movie/"+str(movie.id)+"-"+movie.movie_name+"/index/"
		# movie_json['hash']="#/movie/"+str(movie.id)+"/"+movie.movie_name+"/"
		movie_json['href']="/movie/"+str(movie.id)+"/"+movie.movie_name+"/"
		movie_json['desc'] = 'movie'
		try:
			this_movie_banner_images=banner_images.objects.filter(movie_id=movie.id)
			this_movie_banner_img=random.choice(this_movie_banner_images)
			this_movie_banner_image=this_movie_banner_img.imagefile
			movie_json['icon'] = get_thumbnail(this_movie_banner_image, '80x80', crop='top',quality=99).url
		except banner_images.DoesNotExist:
			movie_json['icon']= get_thumbnail('template_images/no_image.jpg', '80x80', crop='center',quality=99).url
		except Exception, e:
			movie_json['icon']= get_thumbnail('template_images/no_image.jpg', '80x80', crop='center',quality=99).url
		results.append(movie_json)

	personz=movie_persons.objects.filter(person_name__icontains=q)[::-1]
	for person in personz:
		person_json={}
		person_json['id']=person.id
		person_json['label']=person.person_name
		# person_json['url']='/person/'+str(person.id)+'-'+person.person_name+'/index/'
		# person_json['hash']='#/person/'+str(person.id)+'/'+person.person_name+"/"
		person_json['href']='/person/'+str(person.id)+'/'+person.person_name+"/"
		person_json['desc'] = 'person'
		try:
			this_person_profile_images=profile_images.objects.filter(person_id=person.id,image_choosen='T')
			this_person_profile_img=random.choice(this_person_profile_images)
			this_person_profile_image=this_person_profile_img.imagefile
			person_json['icon']=get_thumbnail(this_person_profile_image,'80x80',crop='center',quality=99).url
		except Exception, e:
			#print e
			person_json['icon']= get_thumbnail('template_images/no_image.jpg', '80x80', crop='center',quality=99).url
		results.append(person_json)
		
	data['results'] = results
	data['query'] = q
	data['size'] = len(results)
	return render_to_response(
				'index/templates/all/search_all.html',
				{
				 'results':data,
				},
				context_instance=RequestContext(request))