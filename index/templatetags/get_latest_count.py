from django import template
from movies.models import movie_theater_status

register = template.Library()

# @register.simple_tag
# def get_profile_image40x40(parser):
# 	return pimage_40x40_Node(parser)

@register.filter
def intheaters_count(parser):
	movies_in_theater=movie_theater_status.objects.filter(release_status='T')
	return movies_in_theater.count()

@register.filter
def upcomming_count(parser):
	movies_in_theater=movie_theater_status.objects.filter(release_status='D')
	return movies_in_theater.count()