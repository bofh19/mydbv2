from django.contrib import admin
from django import forms

from movie_persons.models import movie_persons
from movie_persons.models import alternate_names
from movie_persons.models import extra_info
from movie_casting.models import movie_casting
from images.models import profile_images
from wiki_parser.models import person_wlinks

from ajax_select import make_ajax_form
from ajax_select.admin import AjaxSelectAdmin

from sorl.thumbnail import default

ADMIN_THUMBS_SIZE = '40x40'

class alternate_namesinline(admin.TabularInline):
	model=alternate_names
	extra=1

class person_wlinks_inline(admin.TabularInline):
	model=person_wlinks

class profile_imagesinline(admin.TabularInline):
	model=profile_images
	extra=1
	fields = ('imagefile','user_id', 'user_level','image_choosen','image_thumb',)
	readonly_fields=('image_thumb',)

	def image_thumb(self, obj):
		if obj.imagefile:
			thumb = default.backend.get_thumbnail(obj.imagefile.file, ADMIN_THUMBS_SIZE)
			return u'<img width="%s" src="%s" />' % (thumb.width, thumb.url)
		else:
			pass
	image_thumb.short_description = 'My Thumbnail'
	image_thumb.allow_tags = True


class movie_casting_inline(admin.TabularInline):
	model=movie_casting
	form = make_ajax_form(movie_casting,
		{'movie_id':'movies_lookups','profession_id':'professions_lookups'})
	extra=1

class extra_infoinline(admin.TabularInline):	
	model = extra_info
	extra=1

class movie_personsForm(forms.ModelForm):
	person_bio = forms.CharField(widget=forms.Textarea)
	class Meta:
		model = movie_persons

class movie_personsAdmin(admin.ModelAdmin):
	form = movie_personsForm
	inlines = [person_wlinks_inline,movie_casting_inline,profile_imagesinline,alternate_namesinline,extra_infoinline]
	search_fields = ['person_name']

admin.site.register(movie_persons,movie_personsAdmin)
admin.site.register(alternate_names)
admin.site.register(extra_info)