# Create your views here.
from django.shortcuts import render_to_response
from movie_persons.models import movie_persons
from movie_casting.models import movie_casting
from movies.models import movies
from images.models import images
from images.models import images_tags
import json, random
from django.shortcuts import get_object_or_404
from django.template import RequestContext
from django.http import HttpResponse
from django.utils.datastructures import MultiValueDictKeyError
from django.http import HttpResponseRedirect,HttpResponse
from django.core.urlresolvers import reverse
from images.models import profile_images
from professions.models import professions
from django.shortcuts import get_object_or_404
from images.models import banner_images
from movie_persons.models import alternate_names
from movie_persons.models import extra_info



def person_searching(request):
	added_person=''
	if request.is_ajax():
		q=request.GET.get('term','')
		persons=movie_persons.objects.filter(person_name__icontains=q)[:20]
		results=[]
		for person in persons:
			person_json={}
			person_json['id']=person.id
			person_json['label']=person.person_name
			person_json['value']=person.person_name
			results.append(person_json)
		data=json.dumps(results)
	else:
		data='fail'
	mimetype='application/json'
	return HttpResponse(data, mimetype)
