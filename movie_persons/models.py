from django.db import models

# Create your models here.

class movie_persons(models.Model):
	GENDER_CHOICES = (
		(u'M',u'Male'),
		(u'F',u'Female'),
		(u'G',u'Gay'),
		)
	person_name=models.CharField(max_length=400)
	person_gender=models.CharField(max_length=2,choices=GENDER_CHOICES)
	person_date_of_birth=models.DateField(blank=True,null=True,default='1800-01-01')
	person_bio=models.CharField(max_length=2000)
	person_sp=models.CharField(max_length=40,blank=True,null=True)
	person_info = models.TextField(blank=True)
	def __unicode__(self):
		return self.person_name

	def get_absolute_url(self):
		url="/person/"+str(self.id)+"-"+str(self.person_name)+"/index/"
		return url


class alternate_names(models.Model):
	person_id=models.ForeignKey(movie_persons)
	alternate_name=models.CharField(max_length=100)

class extra_info(models.Model):
	person_id=models.ForeignKey(movie_persons)
	fieldone=models.CharField(max_length=400)
	fieldtwo=models.CharField(max_length=2000)