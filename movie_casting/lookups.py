
from django.db.models import Q
from django.utils.html import escape
from movie_persons.models import *
from ajax_select import LookupChannel


class movie_personLookup(LookupChannel):

    model = movie_persons

    def get_query(self,q,request):
        return movie_persons.objects.filter(Q(person_name__icontains=q)).order_by('person_name')

    def get_result(self,obj):
        u""" result is the simple text that is the completion of what the person typed """
        return obj.person_name

    def format_match(self,obj):
        """ (HTML) formatted item for display in the dropdown """
        return self.format_item_display(obj)

    def format_item_display(self,obj):
        """ (HTML) formatted item for displaying item in the selected deck area """
        return u"<div><i>%s</i></div>" % (escape(obj.person_name))
