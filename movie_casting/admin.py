from movie_casting.models import movie_casting
from django.contrib import admin

from ajax_select import make_ajax_form
from ajax_select.admin import AjaxSelectAdmin

class movie_castingAdmin(AjaxSelectAdmin):
	#form = make_ajax_form(Label,{'owner':'person'})
	form = make_ajax_form(movie_casting,{'movie_persons_id':'movie_person_lookups'})

admin.site.register(movie_casting,movie_castingAdmin)



#admin.site.register(movie_casting)