from django.db import models

# Create your models here.
from movies.models import movies
from movie_persons.models import movie_persons
from professions.models import professions

class movie_casting(models.Model):
	movie_id = models.ForeignKey(movies)
	movie_persons_id=models.ForeignKey(movie_persons)
	profession_id=models.ForeignKey(professions)

	class Meta:
		unique_together=('movie_persons_id','movie_id','profession_id')
	def __unicode__(self):
		output = self.movie_id.movie_name + " --- " + self.movie_persons_id.person_name + " --- " + self.profession_id.profession_name
		return output
