from django.db import models

# Create your models here.
from genres.models import genres
from movies.models import movies

class movie_genres(models.Model):
	genre_id=models.ForeignKey(genres)
	movie_id=models.ForeignKey(movies)
	class Meta:
		unique_together=('genre_id','movie_id')
	def __unicode__(self):
		return self.movie_id.movie_name+"---"+self.genre_id.genre_name