from movie_genres.models import movie_genres
from django.contrib import admin

admin.site.register(movie_genres)