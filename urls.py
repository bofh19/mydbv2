# from django.conf.urls.defaults import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls.defaults import *
from ajax_select import urls as ajax_select_urls
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'mydb.views.home', name='home'),
    # url(r'^mydb/', include('mydb.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^parse/',include('wiki_parser.urls')),
    url(r'^music/',include('movie_music.urls')),
    url(r'^v2/',include('indexv2.urls')),
    url(r'^v1/',include('indexv1.urls')),
    url(r'^mobile/v1/',include('mobile_api_v1.urls')),

    url(r'^admin/lookups/', include(ajax_select_urls)),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/', include('registration.backends.default.urls')),
  #  url(r'^movie/(?P<movie_id>\d+)/$','movies.views.movie_detail'),
  #http://127.0.0.1:8000/movie/15-None/
   url(r'^movie/15-(.)?','index.views.movie_default_page'),
)
urlpatterns += patterns('contribute.views',
     url(r'^user/contribute/$','contribute_info'),
)

urlpatterns += patterns('movies.views',

    
    url(r'^movie/search/$','movie_search'),
    url(r'^movie/searching/', 'movie_searching'),
    
	)

urlpatterns += patterns('movie_news.views',

    )

urlpatterns += patterns('movie_music.views',
           
            )


urlpatterns += patterns('user_votes.views',
            # url(r'^movie/(?P<movie_id>\d+)/ajaxrating/', 'ajaxrate'),
            )

urlpatterns += patterns('user_profile.views',
   
)

urlpatterns += patterns('images.views',
    
    )

urlpatterns += patterns('movie_casting.views',
   
    )

urlpatterns += patterns('professions.views',
    url(r'^profession/searching/', 'profession_searching'),
    # url(r'^add/profession/addprof/$', 'add_prof'), #verifying
    )

urlpatterns += patterns('index.views',
    url(r'^$','intheaters',name="intheaters"),
    url(r'^main/$','main_page',name="index_main"), #home button uses this
    url(r'^latest/intheaters/$','intheaters',name="intheaters"),
    url(r'^latest/upcomming/$','upcomming',name="upcomming"),
    url(r'^latest/additions/$','latestdb',name="additions"),

    url(r'^movie/(?P<movie_id>\d+)\/?([^/]?)+/$','m_info',name='movie_info'),
    url(r'^person/(?P<person_id>\d+)\/?([^/]?)+/$','p_info',name='person_info'),

    url(r'^search/all/$','search_all',name="search_all"),    
    )

urlpatterns += patterns('movie_persons.views',
    url(r'^person/searching/', 'person_searching'),
    )

urlpatterns += patterns('user_profile.views',
        url(r'^user/profile/$','user_profile_main',name="user_profile_main"),
	url(r'^user/api/profile/$','user_profile_api',name='user_profile_api'),
       # url(r'^user/profile/redirect/$','redirect_page',name="redirect_page"),
        )
urlpatterns += patterns('',
    url(r'^accounts/logout/$', 'django.contrib.auth.views.logout',name="user_logout"),
    url(r'', include('social_auth.urls')),
    )+static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)

