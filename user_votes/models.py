from django.db import models
from movies.models import movies
from django.contrib.auth.models import User
from movie_ratings.models import rating_cat

class user_votes(models.Model):
	user_id=models.ForeignKey(User)
	movie_id=models.ForeignKey(movies)
	rating=models.IntegerField(choices=((1,'1'),
										(2,'2'),
										(3,'3')
										)
							   )
	rating_cat=models.ForeignKey(rating_cat)
	class Meta:
		unique_together=('user_id','movie_id','rating_cat')

	def __unicode__(self):
		output = self.user_id.username + " -- " + self.movie_id.movie_name + " -- " + str(self.rating) + " -- "+ str(self.rating_cat)
		return output

