from django.shortcuts import render_to_response, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.template import RequestContext

from user_votes.models import user_votes
from movies.models import movies
from movie_ratings.models import movie_ratings
from movie_ratings.models import rating_cat

"""
 1    /  2    /  3
 awesome/routine/boring
 class/ok/mass
 notapplicable/believable/unbelivable
"""

def ajaxrate(request,movie_id):
	try:
		movie_id=movie_id
	except KeyError:
		return HttpResponse("what the hell are you trying to do?")
	if request.user.is_authenticated(): 							# 1
		username=request.user.username
	else:
		return HttpResponse("login before to rate")
	
	uname=request.user
	print uname.id
	outputkeyerror = """
					please fill all values
					"""
	try:
		comedy=request.POST['comedy']
	except KeyError:
		return HttpResponse(outputkeyerror)
	try:
		action=request.POST['action']
	except KeyError:
		return HttpResponse(outputkeyerror)
	try:
		clas=request.POST['class']

		comedy_cat=rating_cat.objects.get(cat_name='comedy')
		try:
			u=user_votes.objects.get(user_id=uname.id,movie_id=movie_id,rating_cat=comedy_cat.id)
			if u:
				if comedy=='comedyone':
					u.rating=1
					u.save()
				elif comedy=='comedytwo':
					u.rating=2
					u.save()
				elif comedy=='comedythree':
					u.rating=3
					u.save()
				#else:
				#	return HttpResponse("please fill all values")
		except user_votes.DoesNotExist:
			u=user_votes()
			u.movie_id=(movies.objects.get(id=movie_id))
			u.user_id=uname
			u.rating_cat=comedy_cat
			if comedy=='comedyone':
				u.rating=1
				u.save()
			elif comedy=='comedytwo':
				print "x"
				u.rating=2
				u.save()
			elif comedy=='comedythree':
				u.rating=3
				u.save()

			print u
	
		action_cat=rating_cat.objects.get(cat_name='action')
		try:
			zu=user_votes.objects.get(user_id=uname.id,movie_id=movie_id,rating_cat=action_cat.id)
			if zu:
				if action=='actionone':
					zu.rating=1
					zu.save()
				elif action=='actiontwo':
					zu.rating=2
					zu.save()
				elif action=='actionthree':
					zu.rating=3
					zu.save()
				#else:
				#	return HttpResponse("please fill all values")
		except user_votes.DoesNotExist:
			zu=user_votes()
			zu.movie_id=(movies.objects.get(id=movie_id))
			zu.user_id=uname
			zu.rating_cat=action_cat
			if action=='actionone':
				zu.rating=1
				zu.save()
			elif action=='actiontwo':
				print "x"
				zu.rating=2
				zu.save()
			elif action=='actionthree':
				zu.rating=3
				zu.save()
			#print u
	
	
		class_cat=rating_cat.objects.get(cat_name='class')
		try:
			au=user_votes.objects.get(user_id=uname.id,movie_id=movie_id,rating_cat=class_cat.id)
			if u:
				if clas=='classone':
					au.rating=1
					au.save()
				elif clas=='classtwo':
					au.rating=2
					au.save()
				elif clas=='classthree':
					au.rating=3
					au.save()
				#else:
				#	return HttpResponse("please fill all values")
		except user_votes.DoesNotExist:
			au=user_votes()
			au.movie_id=(movies.objects.get(id=movie_id))
			au.user_id=uname
			au.rating_cat=class_cat
			if clas=='classone':
				au.rating=1
				au.save()
			elif clas=='classtwo':
				print "x"
				au.rating=2
				au.save()
			elif clas=='classthree':
				au.rating=3
				au.save()
			#print au

	except KeyError:
		return HttpResponse(outputkeyerror)
	
	
	output=comedy+action+clas
	movie_name='-' + movies.objects.get(id=movie_id).movie_name
	return HttpResponse("Thanks for Voting reload to see updated result")
